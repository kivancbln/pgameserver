package controllers

import (
	"encoding/json"
	"net/http"
	"predictiongame/models"
	u "predictiongame/utils"
)

var CreateAccount = func(w http.ResponseWriter, r *http.Request) {

	account := &models.Account{}
	err := json.NewDecoder(r.Body).Decode(account) //decode the request body into struct and failed if any error occur
	if err != nil {
		u.Respond(w, u.Message(false, "Invalid request"))
		return
	}

	resp := account.Create() //Create account
	u.Respond(w, resp)
}

var Authenticate = func(w http.ResponseWriter, r *http.Request) {

	account := &models.Account{}
	err := json.NewDecoder(r.Body).Decode(account) //decode the request body into struct and failed if any error occur
	if err != nil {
		u.Respond(w, u.Message(false, "Invalid request"))
		return
	}

	resp := models.Login(account.Email, account.Password, account.Uuid, account.Serial, account.DeviceModel)
	u.Respond(w, resp)
}

var GetUser = func(w http.ResponseWriter, r *http.Request) {

	data := models.GetUser(u.GetUserId(r))
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

var GetUserByEmail = func(w http.ResponseWriter, r *http.Request) {

	account := &models.Account{}
	err := json.NewDecoder(r.Body).Decode(account) //decode the request body into struct and failed if any error occur
	if err != nil {
		u.Respond(w, u.Message(false, "Invalid request"))
		return
	}

	data := models.GetUserByEmail(account.Email)
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

var GetUserByUUID = func(w http.ResponseWriter, r *http.Request) {

	account := &models.Account{}
	err := json.NewDecoder(r.Body).Decode(account) //decode the request body into struct and failed if any error occur
	if err != nil {
		u.Respond(w, u.Message(false, "Invalid request"))
		return
	}

	data := models.GetUserByUUID(account.Uuid)
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

var ConfirmOrCancel = func(w http.ResponseWriter, r *http.Request) {

	confirmorcancelmodel := &models.ConfirmOrCancelModel{}
	err := json.NewDecoder(r.Body).Decode(confirmorcancelmodel) //decode the request body into struct and failed if any error occur
	if err != nil {
		u.Respond(w, u.Message(false, "Invalid request"))
		return
	}

	resp := models.ConfirmOrCancel(confirmorcancelmodel.Uuid, confirmorcancelmodel.Userid, confirmorcancelmodel.Confirmorcancel)
	u.Respond(w, resp)
}

var UpdateUserName = func(w http.ResponseWriter, r *http.Request) {

	usernameModel := &models.AccountUserNameUpdate{}
	err := json.NewDecoder(r.Body).Decode(usernameModel) //decode the request body into struct and failed if any error occur
	if err != nil {
		u.Respond(w, u.Message(false, "Invalid request"))
		return
	}

	resp := models.UpdateUsername(u.GetUserId(r), usernameModel.UserName)
	u.Respond(w, resp)
}

var UpdateSubscriptionId = func(w http.ResponseWriter, r *http.Request) {

	subscriptionIdModel := &models.AccountSubscriptionIdUpdate{}
	err := json.NewDecoder(r.Body).Decode(subscriptionIdModel) //decode the request body into struct and failed if any error occur
	if err != nil {
		u.Respond(w, u.Message(false, "Invalid request"))
		return
	}

	resp := models.UpdateSubscriptionId(u.GetUserId(r), subscriptionIdModel.SubscriptionId)
	u.Respond(w, resp)
}

var UpdateReferenceID = func(w http.ResponseWriter, r *http.Request) {

	ReferenceIDModel := &models.AccountReferenceUserIDUpdate{}
	err := json.NewDecoder(r.Body).Decode(ReferenceIDModel) //decode the request body into struct and failed if any error occur
	if err != nil {
		u.Respond(w, u.Message(false, "Invalid request"))
		return
	}

	resp := models.UpdateReferenceID(u.GetUserId(r), ReferenceIDModel.ReferenceUserID)
	u.Respond(w, resp)
}
