package controllers

import (
	"encoding/json"
	"predictiongame/models"
	u "predictiongame/utils"
	"net/http"
)

var GetNotifications = func(w http.ResponseWriter, r *http.Request) {

	data := models.GetNotifications(u.GetUserId(r))
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

var CreateNotification = func(w http.ResponseWriter, r *http.Request) {

	notification := &models.Notification{}

	err := json.NewDecoder(r.Body).Decode(notification)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}

	resp := notification.CreateNotification()
	u.Respond(w, resp)
}

var ReadNotification = func(w http.ResponseWriter, r *http.Request) {

	notificationRead := &models.NotificationRead{}

	err := json.NewDecoder(r.Body).Decode(notificationRead)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}

	resp := notificationRead.ReadNotification()
	u.Respond(w, resp)
}