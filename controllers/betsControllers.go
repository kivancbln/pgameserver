package controllers

import (
	"encoding/json"
	"predictiongame/models"
	u "predictiongame/utils"
	"net/http"
	"strconv"
	"fmt"
	"github.com/go-chi/chi"
)

var GetPreviousBetsByUserByMatch = func(w http.ResponseWriter, r *http.Request) {

	userid,err := strconv.ParseUint(r.URL.Query().Get("userId"), 10, 64)
	if err == nil {
		fmt.Println(userid)
	}
	matchid,err := strconv.ParseUint(r.URL.Query().Get("matchId"), 10, 64)
	if err == nil {
		fmt.Println(matchid)
	}
	data := models.GetPreviousBetsByUserByMatch(userid,matchid)
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

var GetPreviousBetsByUser = func(w http.ResponseWriter, r *http.Request) {

	userid,err := strconv.ParseUint(r.URL.Query().Get("userId"), 10, 64)
	if err == nil {
		fmt.Println(userid)
	}
	
	data := models.GetPreviousBetsByUser(userid)
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

var CreateBetByUserByMatch = func(w http.ResponseWriter, r *http.Request) {

	bet := &models.Bet{}

	err := json.NewDecoder(r.Body).Decode(bet)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}

	resp := bet.CreateBetByUserByMatch()
	u.Respond(w, resp)
}

var DeleteBet = func(w http.ResponseWriter, r *http.Request) {

	id,err := strconv.ParseUint(chi.URLParam(r, "id"),10,64)
	if err == nil {
		fmt.Println(id)
	}
	models.DeleteBet(id)
	resp := u.Message(true, "success")
	u.Respond(w, resp)
}

var GetActiveBets = func(w http.ResponseWriter, r *http.Request) {

	data := models.GetActiveBets(u.GetUserId(r))
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

var GetFinishedBets = func(w http.ResponseWriter, r *http.Request) {
	
	data := models.GetFinishedBets(u.GetUserId(r))
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}