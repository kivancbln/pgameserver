package controllers

import (
	"predictiongame/models"
	u "predictiongame/utils"
	"net/http"
	
)


var FetchMatchesFromApi = func(w http.ResponseWriter, r *http.Request) {
	
	date := r.URL.Query().Get("date")
	data := models.FetchMatchesFromApi(date)
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

var FetchMatchesFromApiAndFinish = func(w http.ResponseWriter, r *http.Request) {
	
	date := r.URL.Query().Get("date")
	data := models.FetchMatchesFromApiAndFinish(date)
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

var FetchLeaguesAndSave = func(w http.ResponseWriter, r *http.Request) {
	
	data := models.FetchLeaguesAndSave()
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}