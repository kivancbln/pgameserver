package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"predictiongame/models"
	u "predictiongame/utils"
	"strconv"
)

var GetChallenges = func(w http.ResponseWriter, r *http.Request) {
	userid, err := strconv.ParseUint(r.URL.Query().Get("userId"), 10, 64)
	if err != nil {
		userid = 0
	}
	data := models.GetChallenges(userid)
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

var GetActiveChallenges = func(w http.ResponseWriter, r *http.Request) {
	userid, err := strconv.ParseUint(r.URL.Query().Get("userId"), 10, 64)
	if err != nil {
		userid = 0
	}
	data := models.GetActiveChallenges(userid)
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

var GetActiveChallengesPaged = func(w http.ResponseWriter, r *http.Request) {
	userid, err := strconv.ParseUint(r.URL.Query().Get("userId"), 10, 64)
	if err != nil {
		userid = 0
	}

	page, err2 := strconv.ParseUint(r.URL.Query().Get("page"), 10, 64)
	if err2 != nil {
		page = 1
	}
	offset, err3 := strconv.ParseUint(r.URL.Query().Get("offset"), 10, 64)
	if err3 != nil {
		offset = 10
	}

	data := models.GetActiveChallengesPaged(userid, page, offset)
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

var GetActiveChallengesFromCache = func(w http.ResponseWriter, r *http.Request) {
	userid, err := strconv.ParseUint(r.URL.Query().Get("userId"), 10, 64)
	if err != nil {
		userid = 0
	}
	data := models.GetActiveChallengesFromCache(userid)
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

var GetFinishedChallengesPaged = func(w http.ResponseWriter, r *http.Request) {
	userid, err := strconv.ParseUint(r.URL.Query().Get("userId"), 10, 64)
	if err != nil {
		userid = 0
	}

	page, err2 := strconv.ParseUint(r.URL.Query().Get("page"), 10, 64)
	if err2 != nil {
		page = 1
	}
	offset, err3 := strconv.ParseUint(r.URL.Query().Get("offset"), 10, 64)
	if err3 != nil {
		offset = 10
	}

	data := models.GetFinishedChallengesPaged(userid, page, offset)
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

var GetFinishedChallenges = func(w http.ResponseWriter, r *http.Request) {
	userid, err := strconv.ParseUint(r.URL.Query().Get("userId"), 10, 64)
	if err != nil {
		userid = 0
	}
	data := models.GetFinishedChallenges(userid)
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

var GetChallengeById = func(w http.ResponseWriter, r *http.Request) {
	userid, err := strconv.ParseUint(r.URL.Query().Get("userId"), 10, 64)
	challengeid, err := strconv.ParseUint(r.URL.Query().Get("challengeId"), 10, 64)

	if err != nil {
		userid = 0
	}
	data := models.GetChallengeById(userid, challengeid)
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

var GetResultsByChallenge = func(w http.ResponseWriter, r *http.Request) {

	challengeid, err := strconv.ParseUint(r.URL.Query().Get("challengeId"), 10, 64)

	if err != nil {
		challengeid = 0
	}
	data := models.GetResultsByChallenge(challengeid)
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

var CreateChallenge = func(w http.ResponseWriter, r *http.Request) {

	challenge := &models.Challenge{}

	err := json.NewDecoder(r.Body).Decode(challenge)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		fmt.Println(err)
		return
	}

	resp := challenge.CreateChallenge()
	u.Respond(w, resp)
}

var CreateChallengeMatches = func(w http.ResponseWriter, r *http.Request) {

	matches := &[]models.ChallengeMatch{}

	err := json.NewDecoder(r.Body).Decode(matches)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		fmt.Println(err)
		return
	}
	result := []map[string]interface{}{}

	for _, element := range *matches {
		resp := element.CreateChallengeMatch()
		result = append(result, resp)
	}

	//resp := matches.CreateChallengeMatch()
	u.Respond(w, map[string]interface{}{"status": "true", "message": "success", "data": result})
}

var CreateChallengeContender = func(w http.ResponseWriter, r *http.Request) {

	contender := &models.ChallengeContender{}

	err := json.NewDecoder(r.Body).Decode(contender)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		fmt.Println(err)
		return
	}

	resp := contender.CreateChallengeContender()
	u.Respond(w, resp)
}

var CheckChallengeCompleted = func(w http.ResponseWriter, r *http.Request) {

	contender := &models.ChallengeContender{}

	err := json.NewDecoder(r.Body).Decode(contender)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		fmt.Println(err)
		return
	}

	resp := models.CheckChallengeCompleted(contender.UserId, contender.ChallengeId)
	u.Respond(w, resp)
}

var FinishChallenge = func(w http.ResponseWriter, r *http.Request) {

	challenge := &models.Challenge{}

	err := json.NewDecoder(r.Body).Decode(challenge)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		fmt.Println(err)
		return
	}

	resp := models.FinishChallenge(challenge.ID)
	u.Respond(w, resp)
}
