package controllers

import (
	"encoding/json"
	"net/http"
	"predictiongame/models"
	u "predictiongame/utils"
	"strconv"
	"time"
)

func setupResponse(w *http.ResponseWriter, req *http.Request) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}

var GetMatches = func(w http.ResponseWriter, r *http.Request) {


	newtoken := r.Context().Value("myvalues").(u.Values).Get("newtoken")
	data := models.GetMatches(u.GetUserId(r))
	resp := u.Message(true, "success")
	resp["data"] = data
	resp["newtoken"] = newtoken
	u.Respond(w, resp)
}

var GetMatchesByDate = func(w http.ResponseWriter, r *http.Request) {


	newtoken := r.Context().Value("myvalues").(u.Values).Get("newtoken")
	startdate := r.URL.Query().Get("date")
	enddate, err := time.Parse("2006-01-02T15:04:05.000Z", startdate)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while getting enddate"))
		return
	}
	enddate = enddate.AddDate(0,0,1)
	data := models.GetMatchesByDate(u.GetUserId(r),startdate,enddate.Format("2006-01-02T15:04:05.000Z"))
	resp := u.Message(true, "success")
	resp["data"] = data
	resp["newtoken"] = newtoken
	u.Respond(w, resp)
}

var GetFinishedMatches = func(w http.ResponseWriter, r *http.Request) {


	newtoken := r.Context().Value("myvalues").(u.Values).Get("newtoken")
	data := models.GetFinishedMatches(u.GetUserId(r))
	resp := u.Message(true, "success")
	resp["data"] = data
	resp["newtoken"] = newtoken
	u.Respond(w, resp)
}

var GetFinishedMatchesByDate = func(w http.ResponseWriter, r *http.Request) {


	newtoken := r.Context().Value("myvalues").(u.Values).Get("newtoken")
	startdate := r.URL.Query().Get("date")
	enddate, err := time.Parse("2006-01-02T15:04:05.000Z", startdate)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while getting enddate"))
		return
	}
	enddate = enddate.AddDate(0,0,1)
	data := models.GetFinishedMatchesByDate(u.GetUserId(r),startdate,enddate.Format("2006-01-02T15:04:05.000Z"))
	resp := u.Message(true, "success")
	resp["data"] = data
	resp["newtoken"] = newtoken
	u.Respond(w, resp)
}

var GetMatchById = func(w http.ResponseWriter, r *http.Request) {

	matchid,err := strconv.ParseUint(r.URL.Query().Get("matchId"), 10, 64)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while getting matchid"))
		return
	}
	newtoken := r.Context().Value("myvalues").(u.Values).Get("newtoken")
	data := models.GetMatchById(u.GetUserId(r),matchid)
	resp := u.Message(true, "success")
	resp["data"] = data
	resp["newtoken"] = newtoken
	u.Respond(w, resp)
}

var CreateMatch = func(w http.ResponseWriter, r *http.Request) {

	match := &models.Match{}

	err := json.NewDecoder(r.Body).Decode(match)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}

	resp := match.Create()
	u.Respond(w, resp)
}

var FinishMatch = func(w http.ResponseWriter, r *http.Request) {

	match := &models.Match{}

	err := json.NewDecoder(r.Body).Decode(match)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}

	resp := match.FinishMatch()
	u.Respond(w, resp)
}

var FinishMatch2 = func(w http.ResponseWriter, r *http.Request) {

	match := &models.Match{}

	err := json.NewDecoder(r.Body).Decode(match)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}

	resp := match.FinishMatch2()
	u.Respond(w, resp)
}


var GetRankings = func(w http.ResponseWriter, r *http.Request) {

	year,err := strconv.ParseUint(r.URL.Query().Get("year"), 10, 64)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while getting year"))
		return
	}
	month,err := strconv.ParseUint(r.URL.Query().Get("month"), 10, 64)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while getting month"))
		return
	}

	data := models.GetRankings(uint(year),uint(month),u.GetUserId(r))
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

var CreateMatchOdds = func(w http.ResponseWriter, r *http.Request) {

	matchOdds := &[]models.MatchOdd{}
	
	err := json.NewDecoder(r.Body).Decode(matchOdds)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}
	result := []map[string]interface{}{}
	
	for _, element := range *matchOdds {
		resp := element.CreateMatchOdd()
		result = append(result,resp)
	}
	
	//resp := matches.CreateChallengeMatch()
	u.Respond(w, map[string]interface{}{"status": "true", "message": "success","data":result})
}