package controllers

import (
	"encoding/json"
	"predictiongame/models"
	u "predictiongame/utils"
	"net/http"
)

var CreateTransaction = func(w http.ResponseWriter, r *http.Request) {

	transaction := &models.Transaction{}

	err := json.NewDecoder(r.Body).Decode(transaction)
	if err != nil {
		u.Respond(w, u.Message(false, err.Error()))
		return
	}

	transaction.UserId = u.GetUserId(r)

	resp := transaction.Create()
	u.Respond(w, resp)
}