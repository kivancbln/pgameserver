package controllers

import (
	"encoding/json"
	"predictiongame/models"
	u "predictiongame/utils"
	"net/http"
)

var CreateActionLog = func(w http.ResponseWriter, r *http.Request) {

	actionlog := &models.ActionLog{}
	err := json.NewDecoder(r.Body).Decode(actionlog) //decode the request body into struct and failed if any error occur
	if err != nil {
		u.Respond(w, u.Message(false, "Invalid request"))
		return
	}

	resp := actionlog.Create() //Create actionlog
	u.Respond(w, resp)
}