package controllers

import (
	"encoding/json"
	"net/http"
	"predictiongame/models"
	u "predictiongame/utils"
)

var CreatePrize = func(w http.ResponseWriter, r *http.Request) {

	prize := &models.Prize{}

	err := json.NewDecoder(r.Body).Decode(prize)
	if err != nil {
		u.Respond(w, u.Message(false, err.Error()))
		return
	}

	resp := prize.Create()
	u.Respond(w, resp)
}

var CreateBuyOrder = func(w http.ResponseWriter, r *http.Request) {

	buyorder := &models.BuyOrder{}

	err := json.NewDecoder(r.Body).Decode(buyorder)
	if err != nil {
		u.Respond(w, u.Message(false, err.Error()))
		return
	}

	resp := buyorder.CreateBuyOrder()
	u.Respond(w, resp)
}

// GetBuyOrders controller method to call method
var GetBuyOrders = func(w http.ResponseWriter, r *http.Request) {

	data := models.GetBuyOrders(u.GetUserId(r))
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

// GetBuyables controller to call method
var GetBuyables = func(w http.ResponseWriter, r *http.Request) {

	data := models.GetBuyables()
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

// GetCoin controller to call method
var GetCoin = func(w http.ResponseWriter, r *http.Request) {

	data := models.GetCoin(u.GetUserId(r))
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}
