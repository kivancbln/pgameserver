package controllers

import (
	"encoding/json"
	"predictiongame/models"
	u "predictiongame/utils"
	"net/http"
	"strconv"
	"fmt"
	"log"
)

var CreateGain = func(w http.ResponseWriter, r *http.Request) {

	
	gain := &models.Gain{}

	err := json.NewDecoder(r.Body).Decode(gain)
	if err != nil {
		log.Printf("Body parse error, %v", err)
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}

	
	resp := gain.CreateGain()
	
	u.Respond(w, resp)
}

var GetPoints = func(w http.ResponseWriter, r *http.Request) {

	userid,err := strconv.ParseUint(r.URL.Query().Get("userId"), 10, 64)
	if err == nil {
		fmt.Println(userid)
	}
	data := models.GetPoints(userid)
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

var GetTransactions = func(w http.ResponseWriter, r *http.Request) {

	userid,err := strconv.ParseUint(r.URL.Query().Get("userId"), 10, 64)
	page,err := strconv.ParseUint(r.URL.Query().Get("page"), 10, 64)
	offset,err := strconv.ParseUint(r.URL.Query().Get("offset"), 10, 64)

	if err == nil {
		fmt.Println(userid)
	}
	data := models.GetTransactions(userid,page,offset)
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}
