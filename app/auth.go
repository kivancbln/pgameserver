package app

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"predictiongame/models"
	u "predictiongame/utils"
	"strconv"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
)

type Token struct {
	UserId uint
	exp    int64
	jwt.StandardClaims
}

var JwtAuthentication = func(next http.Handler) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		notAuth := []string{"/api/match/newmatchodds", "/api/user/new", "/index.html", "/api/user/login", "/api/user/getbyemail", "/swagger/doc.json", "/swagger/favicon-16x16.png", "/swagger/favicon-32x32.png", "/swagger/swagger-ui-standalone-preset.js", "/swagger/swagger-ui-bundle.js", "/swagger/swagger-ui.css", "/swagger/index.html", "/socket.io", "/socket.io/"} //List of endpoints that doesn't require auth
		requestPath := r.URL.Path
		if r.URL.String() != "/swagger/index.html" {
			fmt.Println(r.URL)
		}

		//check if request does not need authentication, serve the request if it doesn't need it
		for _, value := range notAuth {

			if value == requestPath {
				next.ServeHTTP(w, r)
				return
			}
		}

		response := make(map[string]interface{})
		tokenHeader := r.Header.Get("Authorization") //Grab the token from the header

		if tokenHeader == "" { //Token is missing, returns with error code 403 Unauthorized
			response = u.Message(false, "Missing auth token")
			w.WriteHeader(http.StatusForbidden)
			w.Header().Add("Content-Type", "application/json")
			u.Respond(w, response)
			return
		}

		splitted := strings.Split(tokenHeader, " ") //The token normally comes in format `Bearer {token-body}`, we check if the retrieved token matched this requirement
		if len(splitted) != 2 {
			response = u.Message(false, "Invalid/Malformed auth token")
			w.WriteHeader(http.StatusForbidden)
			w.Header().Add("Content-Type", "application/json")
			u.Respond(w, response)
			return
		}

		tokenPart := splitted[1] //Grab the token part, what we are truly interested in
		tk := &models.Token{}

		token, err := jwt.ParseWithClaims(tokenPart, tk, func(token *jwt.Token) (interface{}, error) {
			return []byte(os.Getenv("token_password")), nil
		})

		if err != nil { //Malformed token, returns with http code 403 as usual
			response = u.Message(false, err.Error())
			w.WriteHeader(http.StatusForbidden)
			w.Header().Add("Content-Type", "application/json")
			u.Respond(w, response)
			return
		}

		if !token.Valid { //Token is invalid, maybe not signed on this server
			response = u.Message(false, "Token is not valid.")
			w.WriteHeader(http.StatusForbidden)
			w.Header().Add("Content-Type", "application/json")
			u.Respond(w, response)
			return
		}

		//Everything went well, proceed with the request and set the caller to the user retrieved from the parsed token
		expirationTime := time.Now().Add(5 * time.Minute)
		newtk := &Token{UserId: tk.UserId,
			StandardClaims: jwt.StandardClaims{
				// In JWT, the expiry time is expressed as unix milliseconds
				ExpiresAt: expirationTime.Unix(),
			}}
		newtoken := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), newtk)
		newtokenString, _ := newtoken.SignedString([]byte(os.Getenv("token_password")))
		fmt.Sprintf("User %", tk.UserId) //Useful for monitoring
		v := u.Values{M: map[string]string{
			"user":     strconv.FormatUint(uint64(tk.UserId), 10),
			"newtoken": newtokenString,
		}}
		ctx := context.WithValue(r.Context(), "myvalues", v)
		r = r.WithContext(ctx)

		go models.UpdateLastLoginDate(tk.UserId)
		next.ServeHTTP(w, r) //proceed in the middleware chain!
	})
}
