module predictiongame

require (
	github.com/ahmetb/go-linq v3.0.0+incompatible
	github.com/cosiner/argv v0.0.1 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v4.1.1+incompatible
	github.com/go-delve/delve v1.3.2 // indirect
	github.com/go-openapi/spec v0.19.8 // indirect
	github.com/go-openapi/swag v0.19.9 // indirect
	github.com/googollee/go-engine.io v1.4.3-0.20200220091802-9b2ab104b298
	github.com/googollee/go-socket.io v1.4.3
	github.com/gorilla/context v1.1.1 // indirect
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.3
	github.com/gorilla/websocket v1.4.2
	github.com/jinzhu/gorm v1.9.12
	github.com/joho/godotenv v1.3.0
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/lib/pq v1.5.2 // indirect
	github.com/mailru/easyjson v0.7.1 // indirect
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-isatty v0.0.10 // indirect
	github.com/mattn/go-runewidth v0.0.7 // indirect
	github.com/mitchellh/mapstructure v1.3.0
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/peterh/liner v1.1.0 // indirect
	github.com/robfig/cron v1.2.0
	github.com/rs/cors v1.7.0 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.4.2 // indirect
	github.com/spf13/cobra v0.0.5 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/swaggo/http-swagger v0.0.0-20200308142732-58ac5e232fba
	github.com/swaggo/swag v1.6.5 // indirect
	go.starlark.net v0.0.0-20191202231402-1e82a9dd93ba // indirect
	golang.org/x/arch v0.0.0-20191126211547-368ea8f32fff // indirect
	golang.org/x/crypto v0.0.0-20200510223506-06a226fb4e37
	golang.org/x/net v0.0.0-20200513185701-a91f0712d120 // indirect
	golang.org/x/tools v0.0.0-20200515220128-d3bf790afa53 // indirect
	google.golang.org/appengine v1.6.4 // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
)

go 1.13
