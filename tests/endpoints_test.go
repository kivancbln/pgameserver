package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"predictiongame/controllers"
	"testing"
)

func TestAuthenticate(t *testing.T) {

	var jsonStr = []byte(`{"email":"kivanc.bilen@icloud.com","password":"300300Kb"}`)
	req, err := http.NewRequest("POST", "/api/user/login", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(controllers.Authenticate)
	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	fmt.Println(rr.Body.String())

	pingJSON := make(map[string]interface{})
	err2 := json.Unmarshal([]byte(rr.Body.String()), &pingJSON)

	if err2 != nil {
		panic(err2)
	}

	// Check the response body is what we expect.
	if pingJSON["status"] != true {
		t.Errorf("handler returned unexpected body: got %v want %v",
			pingJSON["status"], true)
	}
}
