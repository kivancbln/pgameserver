package utils

import (
	"encoding/json"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/patrickmn/go-cache"
)

type Token2 struct {
	UserId uint
	exp    int64
	jwt.StandardClaims
}

type Values struct {
	M map[string]string
}

var (
	tc = cache.New(23*time.Hour, 23*time.Hour)
)

func (v Values) Get(key string) string {
	return v.M[key]
}

func Message(status bool, message string) map[string]interface{} {
	return map[string]interface{}{"status": status, "message": message}
}

func Respond(w http.ResponseWriter, data map[string]interface{}) {
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}

func GetUserId(r *http.Request) uint {
	tokenHeader := r.Header.Get("Authorization")
	splitted := strings.Split(tokenHeader, " ")
	tokenPart := splitted[1]
	tk := &Token2{}

	jwt.ParseWithClaims(tokenPart, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(os.Getenv("token_password")), nil
	})

	return tk.UserId
}

func GetCache() *cache.Cache {
	return tc
}
