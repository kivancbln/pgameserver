package models

import (
	"net/http"
	"fmt"
	"strings"
	"io/ioutil"
	u "predictiongame/utils"

)

type SubscriptionModel struct {
	SubscriptionId string `json:"subscriptionId"`
}

func SendNotifications(date1 string,date2 string) (map[string]interface{}) {
	subs := make([]*SubscriptionModel , 0)


	s:=fmt.Sprintf(`select distinct a.subscription_id from bets b
	inner join matches m on m.id = b.match_id
	inner join accounts a on b.user_id = a.id
	where m.date >= '%s' and m.date <'%s'
	and coalesce(a.subscription_id,'') != ''
	and b.won=true`,date1,date2);
	err := GetDB().Raw(s).Scan(&subs).Error
	
	subs2 := []string{}
	for _, sub := range subs {
		subs2 = append(subs2,sub.SubscriptionId )
	}
	subsCommaSeperated := strings.Join(subs2 ,"\",\"")
	subsCommaSeperated = "\"" + subsCommaSeperated + "\""
	
	url := "https://onesignal.com/api/v1/notifications"
	method := "POST"

	postJson:=fmt.Sprintf(`{"app_id": "ba4bc4e7-0547-49b9-8c16-f1f8d9b4225a",
							"contents": { "en": "Tebrikler kazandınız"},
							"include_player_ids": [
								%s
								]
							}`,subsCommaSeperated);

	payload := strings.NewReader(postJson)

	client := &http.Client {
	}
	req, err := http.NewRequest(method, url, payload)

	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("Content-Type", "application/json; charset=utf-8")
	req.Header.Add("Authorization", "Basic MTI2YjU1YjgtMDQxMi00YzA1LWJkMDEtZTZmYmNiYjIyN2Rj")
	// req.Header.Add("Cookie", "__cfduid=d2abf6d9a9408b8f4c2ce4d94f72411511595152136")

	res, err := client.Do(req)
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	fmt.Println(string(body))
	resp := u.Message(true, "success")
	resp["data"]=string(body)
	return resp
}