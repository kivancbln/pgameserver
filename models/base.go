package models

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/joho/godotenv"
)

var db *gorm.DB

func init() {

	e := godotenv.Load(os.ExpandEnv("/home/ec2-user/go/src/predictiongame/.env"))
	if e != nil {
		fmt.Print(e)
	}

	username := os.Getenv("db_user")
	password := os.Getenv("db_pass")
	dbName := os.Getenv("db_name")
	dbHost := os.Getenv("db_host")

	dbUri := fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable password=%s", dbHost, username, dbName, password)
	fmt.Println(dbUri)

	conn, err := gorm.Open("postgres", dbUri)

	if err != nil {
		fmt.Print(err)
	}

	conn.LogMode(false)
	db = conn

	db.Debug().AutoMigrate(&Account{}, &Contact{}, &Match{}, &Bet{}, &Gain{}, &Challenge{}, &ChallengeMatch{}, &ChallengeContender{}, &Prize{}, &Winning{}, &PrizeType{}, &TotalOrEach{}, &League{}, &Transaction{}, &ActionLog{}, &Ranking{}, &Notification{}, &NotificationRead{}, &MatchOdd{}, &Customer{}, &Buyable{}, &BuyOrder{}, &GeneralSetting{})
	db.Model(&ChallengeMatch{}).AddUniqueIndex("idx_chid_mid", "challenge_id", "match_id")
	db.Model(&ChallengeContender{}).AddUniqueIndex("idx_chid_userid", "challenge_id", "user_id")
	db.Model(&PrizeType{}).AddUniqueIndex("idx_ptype_desc", "description")
	db.Model(&TotalOrEach{}).AddUniqueIndex("idx_tore_desc", "description")
	db.Model(&Match{}).AddUniqueIndex("idx_sourceid", "source_id")
	db.Model(&League{}).AddUniqueIndex("idx_leagueid", "league_id")
	db.Model(&NotificationRead{}).AddUniqueIndex("idx_chid_userid", "notification_id", "user_id")
	db.Model(&MatchOdd{}).AddUniqueIndex("idx_fix_oddV_oddD", "fixture_id", "odd_desc", "odd_value")

	ptype := PrizeType{Description: "points"}
	db.Create(&ptype)
	ptype = PrizeType{Description: "money"}
	db.Create(&ptype)
	ptype = PrizeType{Description: "customer coupon money"}
	db.Create(&ptype)
	ptype = PrizeType{Description: "customer coupon percent"}
	db.Create(&ptype)
	ptype = PrizeType{Description: "coins"}
	db.Create(&ptype)

	tore := TotalOrEach{Description: "total"}
	db.Create(&tore)
	tore = TotalOrEach{Description: "each"}
	db.Create(&tore)

}

func GetDB() *gorm.DB {
	return db
}
