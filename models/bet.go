package models

import (
	u "predictiongame/utils"

	"github.com/jinzhu/gorm"
)

type Bet struct {
	gorm.Model
	UserId      uint64   `json:"userId"`
	MatchId     uint64   `json:"matchId"`
	HomeScore   uint     `json:"homeScore"`
	AwayScore   uint     `json:"awayScore"`
	Points      uint     `json:"points"`
	ChallengeId uint64   `json:"challengeId"`
	Won         bool     `json:"won" gorm:"default:false"`
	Factor      float32  `json:"factor" gorm:"default:1"`
	OddId       uint64   `json:"oddId"`
	Odd         MatchOdd `gorm:"FOREIGNKEY:ID;ASSOCIATION_FOREIGNKEY:OddId"`
	Match       Match    `gorm:"FOREIGNKEY:ID;ASSOCIATION_FOREIGNKEY:MatchId"`
	Gain        Gain     `gorm:"FOREIGNKEY:BetId;ASSOCIATION_FOREIGNKEY:ID"`
}

type BetWithMatch struct {
	gorm.Model
	UserId       uint64   `json:"userId"`
	MatchId      uint64   `json:"matchId"`
	HomeScore    uint     `json:"homeScore"`
	AwayScore    uint     `json:"awayScore"`
	Points       uint     `json:"points"`
	ChallengeId  uint64   `json:"challengeId"`
	Won          bool     `json:"won"`
	Home         string   `json:"home"`
	Away         string   `json:"away"`
	GainedPoints float32  `json:"gainedPoints"`
	Odd          MatchOdd `gorm:"FOREIGNKEY:ID;ASSOCIATION_FOREIGNKEY:OddId"`
}

func (bet *Bet) Validate() (map[string]interface{}, bool) {

	if bet.UserId == 0 {
		return u.Message(false, "UserId should be filled"), false
	}

	if bet.MatchId == 0 {
		return u.Message(false, "MatchId should be filled"), false
	}

	if bet.Points == 0 {
		return u.Message(false, "Points should be filled"), false
	}

	if bet.OddId == 0 {
		return u.Message(false, "Odd Id must be filled"), false
	}

	//All the required parameters are present
	return u.Message(true, "success"), true
}

func GetPreviousBetsByUserByMatch(userid uint64, matchid uint64) []*Bet {

	bets := make([]*Bet, 0)
	err := GetDB().Table("bets").Preload("Odd").Where("user_id = ?", userid).Where("match_id = ?", matchid).Find(&bets).Error
	if err != nil {
		return nil
	}
	return bets
}

func GetPreviousBetsByUser(userid uint64) []*Bet {

	betswithmatch := make([]*Bet, 0)
	err := GetDB().Model("bets").Preload("Odd").Preload("Match").Where("user_id = ?", userid).Find(&betswithmatch).Error
	//err := GetDB().Table("bets").Where("user_id = ?",userid).Where("match_id = ?",matchid).Find(&betswithmatch).Error
	if err != nil {
		return nil
	}
	return betswithmatch
}

func (bet *Bet) CreateBetByUserByMatch() map[string]interface{} {

	if resp, ok := bet.Validate(); !ok {
		return resp
	}

	match := &Match{}
	GetDB().Table("matches").Where("id = ?", bet.MatchId).First(&match)

	acc := &Account{}
	err := GetDB().Table("accounts").Where("id = ?", bet.UserId).First(acc).Error

	if err != nil {
		return u.Message(false, "User cannot be found")
	}
	if (acc.Points - float32(bet.Points)) < 0 {
		return u.Message(false, "Insufficient points")
	}

	bet.Factor = bet.Factor * match.Factor

	GetDB().Create(bet)
	PlusOrMinusPoints(bet.UserId, -1.0*float32(bet.Points))

	resp := u.Message(true, "success")
	resp["bet"] = bet
	return resp
}

func DeleteBet(betid uint64) map[string]interface{} {
	bet := &Bet{}
	err := GetDB().Table("bets").Where("id = ?", betid).First(&bet).Error
	if err != nil {
		return nil
	}
	GetDB().Delete(Bet{}, "id = ?", betid)

	PlusOrMinusPoints(bet.UserId, float32(bet.Points))

	resp := u.Message(true, "success")
	return resp
}

func GetActiveBets(userid uint) []*Bet {

	betswithmatch := make([]*Bet, 0)
	err := GetDB().Table("bets").Preload("Odd").Preload("Match").Where("user_id = ?", userid).Where("EXISTS (SELECT 1 FROM matches WHERE matches.ID = bets.match_id AND matches.finished = false)").Order("created_at desc").Find(&betswithmatch).Error

	//err := GetDB().Table("bets").Where("user_id = ?",userid).Where("match_id = ?",matchid).Find(&betswithmatch).Error
	if err != nil {
		return nil
	}
	return betswithmatch
}

func GetFinishedBets(userid uint) []*Bet {

	betswithmatch := make([]*Bet, 0)
	err := GetDB().Table("bets").Preload("Odd").Preload("Match").Preload("Gain").Where("user_id = ?", userid).Where("EXISTS (SELECT 1 FROM matches WHERE matches.ID = bets.match_id AND matches.finished = true)").Order("created_at desc").Find(&betswithmatch).Error
	//err := GetDB().Table("bets").Where("user_id = ?",userid).Where("match_id = ?",matchid).Find(&betswithmatch).Error
	if err != nil {
		return nil
	}
	return betswithmatch
}
