package models

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	//"log"
	"reflect"
	"strconv"
	"time"

	"github.com/mitchellh/mapstructure"
)

type ApiResult struct {
	api Api `json:"api"`
}

type Api struct {
	results  int       `json:"results"`
	fixtures []Fixture `json:"fixtures"`
}

type Fixture struct {
	fixture_id      uint64 `json:"fixture_id"`
	league_id       uint64 `json:"league_id"`
	event_timestamp uint64 `json:"event_timestamp"`
}

func FetchMatchesFromApi(date string) ApiResult {

	result := ApiResult{}

	url := "https://api-football-v1.p.rapidapi.com/v2/fixtures/date/" + date + "?timezone=Europe%2FLondon"

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("x-rapidapi-host", "api-football-v1.p.rapidapi.com")
	req.Header.Add("x-rapidapi-key", "CxLfehQqv8mshy78orjEbBDS8L2Dp1GtZQCjsnbEVKKUjxcfsi")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	str := string(body)

	/*str := `{
		"api":{
		   "results":63,
		   "fixtures":[
			  {
				 "fixture_id":239076,
				 "league_id":909,
				 "league":{
					"name":"Birinci Dasta",
					"country":"Azerbaidjan",
					"logo":null,
					"flag":"https://media.api-football.com/flags/az.svg"
				 },
				 "event_date":"2020-02-06T00:00:00+00:00",
				 "event_timestamp":1580947200,
				 "firstHalfStart":null,
				 "secondHalfStart":null,
				 "round":"Regular Season - 14",
				 "status":"Match Cancelled",
				 "statusShort":"CANC",
				 "elapsed":0,
				 "venue":null,
				 "referee":null,
				 "homeTeam":{
					"team_id":5489,
					"team_name":"Ağsu",
					"logo":"https://media.api-football.com/teams/5489.png"
				 },
				 "awayTeam":{
					"team_id":5501,
					"team_name":"Zirə II",
					"logo":"https://media.api-football.com/teams/5501.png"
				 },
				 "goalsHomeTeam":null,
				 "goalsAwayTeam":null,
				 "score":{
					"halftime":null,
					"fulltime":null,
					"extratime":null,
					"penalty":null
				 }
			  },
			  {
				 "fixture_id":310830,
				 "league_id":586,
				 "league":{
					"name":"Liga Nacional",
					"country":"Guatemala",
					"logo":null,
					"flag":"https://media.api-football.com/flags/gt.svg"
				 },
				 "event_date":"2020-02-06T00:00:00+00:00",
				 "event_timestamp":1580947200,
				 "firstHalfStart":1580947200,
				 "secondHalfStart":1580950800,
				 "round":"Clausura - 5",
				 "status":"Match Finished",
				 "statusShort":"FT",
				 "elapsed":90,
				 "venue":"Estadio Municipal Mateo Sicay Paz",
				 "referee":null,
				 "homeTeam":{
					"team_id":3666,
					"team_name":"Siquinalá",
					"logo":"https://media.api-football.com/teams/3666.png"
				 },
				 "awayTeam":{
					"team_id":3662,
					"team_name":"Guastatoya",
					"logo":"https://media.api-football.com/teams/3662.png"
				 },
				 "goalsHomeTeam":2,
				 "goalsAwayTeam":1,
				 "score":{
					"halftime":"1-0",
					"fulltime":"2-1",
					"extratime":null,
					"penalty":null
				 }
			  }
		   ]
		}
	 }`	*/
	//json.Unmarshal([]byte(str), &result)
	//fmt.Println(result)

	//str := `{"page": 1, "fruits": ["apple", "peach"]}`
	//res := response2{}
	//json.Unmarshal([]byte(str), &res)
	//fmt.Println(res)

	var f interface{}
	json.Unmarshal([]byte(str), &f)

	m := f.(map[string]interface{})

	foomap := m["api"]
	v := foomap.(map[string]interface{})

	foofixtures := v["fixtures"]
	fixtures := foofixtures.([]interface{})

	for i, fixture := range fixtures {
		match := Match{}
		fmt.Println(i)
		hometeam := fixture.(map[string]interface{})["homeTeam"]
		awayteam := fixture.(map[string]interface{})["awayTeam"]
		match.Home = hometeam.(map[string]interface{})["team_name"].(string)
		match.Away = awayteam.(map[string]interface{})["team_name"].(string)
		match.LeagueId = fixture.(map[string]interface{})["league_id"].(float64)
		fmt.Println(fixture.(map[string]interface{})["event_date"].(string))
		dt, errdate := time.Parse("2006-01-02T15:04:05Z07:00", fixture.(map[string]interface{})["event_date"].(string))
		if errdate != nil {
			fmt.Println(errdate)
		}
		match.Date = dt
		match.SourceId = uint64(fixture.(map[string]interface{})["fixture_id"].(float64))
		match.Status = fixture.(map[string]interface{})["statusShort"].(string)
		if match.Status != "PST" && match.Status != "CANC" {
			GetDB().Create(&match)
		}
	}

	return result
}

func FetchMatchesFromApiAndFinish(date string) ApiResult {

	result := ApiResult{}

	url := "https://api-football-v1.p.rapidapi.com/v2/fixtures/date/" + date + "?timezone=Europe%2FLondon"

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("x-rapidapi-host", "api-football-v1.p.rapidapi.com")
	req.Header.Add("x-rapidapi-key", "CxLfehQqv8mshy78orjEbBDS8L2Dp1GtZQCjsnbEVKKUjxcfsi")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	str := string(body)

	var f interface{}
	json.Unmarshal([]byte(str), &f)

	m := f.(map[string]interface{})

	foomap := m["api"]
	v := foomap.(map[string]interface{})

	foofixtures := v["fixtures"]
	fixtures := foofixtures.([]interface{})

	for i, fixture := range fixtures {
		match := &Match{}
		//fmt.Println(i)
		status := fixture.(map[string]interface{})["statusShort"].(string)
		var id uint64
		GetDB().Table("matches").Select("id").Where("source_id = ? ", uint64(fixture.(map[string]interface{})["fixture_id"].(float64))).Row().Scan(&id)
		if id > 0 {
			//fmt.Println("entered")
			if status == "FT" || status == "AET" || status == "PEN" {
				fmt.Println("ft entered ", i, id)
				match.ID = uint(id)
				hometeam := fixture.(map[string]interface{})["homeTeam"]
				awayteam := fixture.(map[string]interface{})["awayTeam"]
				match.Home = hometeam.(map[string]interface{})["team_name"].(string)
				match.Away = awayteam.(map[string]interface{})["team_name"].(string)
				//match.HomeScore = uint(fixture.(map[string]interface{})["goalsHomeTeam"].(float64))
				//match.AwayScore = uint(fixture.(map[string]interface{})["goalsAwayTeam"].(float64))
				score := fixture.(map[string]interface{})["score"]

				if score.(map[string]interface{})["fulltime"] != nil {
					scores := strings.Split(score.(map[string]interface{})["fulltime"].(string), "-")

					hs, err := strconv.ParseUint(scores[0], 10, 32)
					if err != nil {
						fmt.Println("error")
					}
					as, err := strconv.ParseUint(scores[1], 10, 32)
					if err != nil {
						fmt.Println("error")
					}
					match.HomeScore = uint(hs)
					match.AwayScore = uint(as)

					fmt.Println(match.Home + " - " + match.Away)
					fmt.Println(match.HomeScore)
					match.Finished = true
					match.Status = status
					match.FinishMatch2()
				}
			} else {
				//fmt.Println("not ft entered")
				match.ID = uint(id)
				match.Status = status
				match.CancelOrPostponeMatch()
			}
		}

	}

	s := fmt.Sprintf(`delete from rankings where year = EXTRACT(YEAR from to_date('%s','yyyy-MM-dd')) and month = EXTRACT(MONTH from to_date('%s','yyyy-MM-dd'));
	insert into rankings(created_at,rank,user_id,user_name,points,year,month)
	select now(),
	RANK () OVER (ORDER BY sum(g.gained_points) desc) rank_number,
	g.user_id,
	a.user_name,
	to_number(to_char(sum(g.gained_points), 'L9G999g999.99'),'L9G999g999.99') points,
	EXTRACT(YEAR from to_date('%s','yyyy-MM-dd')),
	EXTRACT(MONTH from to_date('%s','yyyy-MM-dd'))
	from gains g
	inner join matches m on g.match_id = m.id
	inner join accounts a on a.id = g.user_id
	where EXTRACT(MONTH FROM m.date) = EXTRACT(MONTH from to_date('%s','yyyy-MM-dd'))
	group by g.user_id,a.user_name
	order by sum(gained_points) desc;
	
	select * from challenges limit(1);`, date, date, date, date, date)
	challenges := make([]*Challenge, 0)
	GetDB().Raw(s).Scan(&challenges)

	return result
}

func FetchLeaguesAndSave() ApiResult {

	result := ApiResult{}

	url := "https://api-football-v1.p.rapidapi.com/v2/leagues/current"

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("x-rapidapi-host", "api-football-v1.p.rapidapi.com")
	req.Header.Add("x-rapidapi-key", "CxLfehQqv8mshy78orjEbBDS8L2Dp1GtZQCjsnbEVKKUjxcfsi")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	str := string(body)

	var f interface{}
	json.Unmarshal([]byte(str), &f)

	m := f.(map[string]interface{})

	foomap := m["api"]
	v := foomap.(map[string]interface{})

	fooleagues := v["leagues"]
	leagues := fooleagues.([]interface{})

	stringToDateTimeHook := func(
		f reflect.Type,
		t reflect.Type,
		data interface{}) (interface{}, error) {
		if t == reflect.TypeOf(time.Time{}) && f == reflect.TypeOf("") {
			return time.Parse("2006-01-02", data.(string))
		}

		return data, nil
	}

	for i, lg := range leagues {
		fmt.Println(i)
		league := &League{}

		//mapstructure.Decode(lg, &league)

		config := mapstructure.DecoderConfig{
			DecodeHook: stringToDateTimeHook,
			Result:     &league,
		}

		decoder, err := mapstructure.NewDecoder(&config)
		if err != nil {
			fmt.Println(err.Error())
			return result
		}

		err = decoder.Decode(lg)
		if err != nil {
			fmt.Println(err.Error())
			return result
		}

		league.Create()

	}

	return result
}
