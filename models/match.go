package models

import (
	"fmt"
	u "predictiongame/utils"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

type Match struct {
	gorm.Model
	Home         string     `json:"home"`
	Away         string     `json:"away"`
	Date         time.Time  `json:"date"` //The user that this contact belongs to
	HomeScore    uint       `json:"homescore"`
	AwayScore    uint       `json:"awayscore"`
	Finished     bool       `json:"finished" gorm:"default:false"`
	FinishedDate time.Time  `json:"finishedDate"`
	SourceId     uint64     `json:"sourceId"`
	LeagueId     float64    `json:"leagueId"`
	Status       string     `json:"status"`
	Factor       float32    `json:"factor" gorm:"default:1"`
	Bets         []Bet      `gorm:"FOREIGNKEY:MatchId;ASSOCIATION_FOREIGNKEY:ID"`
	League       League     `gorm:"FOREIGNKEY:LeagueId;ASSOCIATION_FOREIGNKEY:LeagueId"`
	MatchOdds    []MatchOdd `gorm:"FOREIGNKEY:FixtureId;ASSOCIATION_FOREIGNKEY:SourceId"`
}

type Ranking struct {
	gorm.Model
	Rank     uint    `json:"rank"`
	UserId   uint    `json:"userId"`
	UserName string  `json:"userName"`
	Points   float64 `json:"points"`
	Year     uint    `json:"year"`
	Month    uint    `json:"month"`
}

type RankingReturn struct {
	Rankings    []Ranking
	UserRanking uint    `json:"userRanking"`
	UserPoints  float64 `json:"userPoints"`
	Year        uint    `json:"year"`
	Month       uint    `json:"month"`
}

type MatchOdd struct {
	gorm.Model
	FixtureId uint64  `json:"fixtureId"`
	OddDesc   string  `json:"oddDesc"`
	OddValue  string  `json:"oddValue"`
	Rate      float64 `json:"rate"`
}

type PrioritySorter []*Match

func (a PrioritySorter) Len() int           { return len(a) }
func (a PrioritySorter) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a PrioritySorter) Less(i, j int) bool { return a[i].League.Priority > a[j].League.Priority }

func (match *Match) Validate() (map[string]interface{}, bool) {

	if match.Home == "" {
		return u.Message(false, "Home should be filled"), false
	}

	if match.Away == "" {
		return u.Message(false, "Away should be filled"), false
	}

	//All the required parameters are present
	return u.Message(true, "success"), true
}

func GetMatches(userid uint) []*Match {

	matches := make([]*Match, 0)
	err := GetDB().Model("matches").Preload("Bets", "user_id = ?", userid).Preload("League").Where("finished = false").Find(&matches).Error
	if err != nil {
		return nil
	}
	return matches
}

func GetMatchesByDate(userid uint, startdate string, enddate string) []*Match {

	matches := make([]*Match, 0)
	err := GetDB().Model("matches").Preload("Bets", "user_id = ?", userid).Preload("Bets.Odd").Preload("League").Preload("MatchOdds").Where("finished = false and date >= ? and date < ?", startdate, enddate).Where("EXISTS (select 1 from match_odds where fixture_id = matches.source_id)").Find(&matches).Error
	if err != nil {
		return nil
	}
	sort.Sort(PrioritySorter(matches))
	return matches
}

func GetFinishedMatches(userid uint) []*Match {

	matches := make([]*Match, 0)
	err := GetDB().Model("matches").Preload("Bets", "user_id = ?", userid).Preload("League").Where("finished = true").Where("EXISTS (select 1 from match_odds where fixture_id = matches.source_id)").Find(&matches).Error
	if err != nil {
		return nil
	}
	return matches
}

func GetFinishedMatchesByDate(userid uint, startdate string, enddate string) []*Match {

	matches := make([]*Match, 0)
	err := GetDB().Model("matches").Preload("Bets", "user_id = ?", userid).Preload("Bets.Odd").Preload("League").Preload("MatchOdds").Where("finished = true and date >= ? and date < ?", startdate, enddate).Where("EXISTS (select 1 from match_odds where fixture_id = matches.source_id)").Find(&matches).Error
	if err != nil {
		return nil
	}
	sort.Sort(PrioritySorter(matches))
	return matches
}

func GetMatchById(userid uint, matchid uint64) []*Match {

	matches := make([]*Match, 0)
	err := GetDB().Model("matches").Preload("Bets", "user_id = ?", userid).Preload("Bets.Odd").Preload("League").Preload("MatchOdds").Where("finished = false and ID = ?", matchid).Find(&matches).Error
	if err != nil {
		return nil
	}
	return matches
}

func (match *Match) Create() map[string]interface{} {

	if resp, ok := match.Validate(); !ok {
		return resp
	}

	GetDB().Create(match)

	resp := u.Message(true, "success")
	resp["match"] = match
	return resp
}

func (matchOdd *MatchOdd) CreateMatchOdd() map[string]interface{} {

	GetDB().Create(matchOdd)

	resp := u.Message(true, "success")
	resp["matchOdd"] = matchOdd
	return resp
}

func (match *Match) FinishMatch() map[string]interface{} {

	bets := make([]*Bet, 0)
	//winningBets := make([]*Gain, 0)

	//allbets := make([]*Bet, 0)

	if resp, ok := match.Validate(); !ok {
		return resp
	}

	var sumpoints uint
	var winningpoints uint
	var isFinished bool
	winningpoints = 0
	sumpoints = 0
	isFinished = false

	//check if match already finished
	GetDB().Table("matches").Select("finished").Where("ID = ? ", match.ID).Row().Scan(&isFinished)
	if isFinished {
		resp := u.Message(true, "already finished")
		resp["match"] = match
		return resp
	}
	//sum of all bet points
	GetDB().Table("bets").Select("sum(points)").Where("match_id = ? ", match.ID).Row().Scan(&sumpoints)

	//get all winning bets
	GetDB().Table("bets").Where("match_id = ? and home_score = ? and away_score = ?", match.ID, match.HomeScore, match.AwayScore).Find(&bets)

	//get winning bets total points and set won flag
	for _, element := range bets {
		winningpoints = winningpoints + element.Points
		GetDB().Model(&element).Update("won", true)
	}

	//rate calculated
	if winningpoints > 0 {
		var rate float32
		rate = float32(sumpoints) / float32(winningpoints)

		//gains inserted
		for _, element := range bets {
			wBet := &Gain{}
			wBet.UserId = element.UserId
			wBet.MatchId = element.MatchId
			wBet.ChallengeId = element.ChallengeId
			wBet.BetId = element.ID
			wBet.Type = 1
			wBet.GainedPoints = float32(element.Points) * rate * element.Factor
			GetDB().Create(wBet)
			PlusOrMinusPoints(wBet.UserId, wBet.GainedPoints)
		}
	}

	//match finished
	GetDB().Model(&match).Updates(map[string]interface{}{"finished": true, "finished_date": match.FinishedDate, "status": match.Status, "home_score": match.HomeScore, "away_score": match.AwayScore})

	resp := u.Message(true, "success")
	resp["match"] = match
	return resp
}

func (match *Match) FinishMatch2() map[string]interface{} {

	bets := make([]*Bet, 0)

	if resp, ok := match.Validate(); !ok {
		return resp
	}

	var isFinished bool
	isFinished = false

	//check if match already finished
	GetDB().Table("matches").Select("finished").Where("ID = ? ", match.ID).Row().Scan(&isFinished)
	if isFinished {
		resp := u.Message(true, "already finished")
		resp["match"] = match
		return resp
	}

	var str strings.Builder

	str.WriteString(strconv.FormatUint(uint64(match.HomeScore), 10))
	str.WriteString(":")
	str.WriteString(strconv.FormatUint(uint64(match.AwayScore), 10))

	var exactScore = str.String()

	var matchWinner string

	if match.HomeScore > match.AwayScore {
		matchWinner = "Home"
	} else if match.AwayScore > match.HomeScore {
		matchWinner = "Away"
	} else {
		matchWinner = "Draw"
	}

	s := fmt.Sprintf(`INSERT INTO public.gains(
		created_at, updated_at, user_id, match_id, 
		type, gained_points, challenge_id, bet_id)
	select now(), now(),b.user_id,b.match_id,1,b.points*mo.rate*b.factor
	,b.challenge_id,b.id
	 from matches m
	 inner join match_odds mo on mo.fixture_id = m.source_id
	inner join bets b on b.match_id = m.id and b.odd_id= mo.id
	where m.id=%d
	and ((mo.odd_desc = 'Match Winner' and mo.odd_value = '%s') or (mo.odd_desc = 'Exact Score' and mo.odd_value = '%s'));
	
	select distinct * from bets b 
	inner join gains g on g.bet_id = b.id
	where g.deleted_at is null and  b.match_id = %d;
	`, match.ID, matchWinner, exactScore, match.ID)

	err := GetDB().Raw(s).Scan(&bets).Error
	if err != nil {
		resp := u.Message(false, err.Error())
		return resp
	}

	//get winning bets total points and set won flag
	for _, element := range bets {
		GetDB().Model(&element).Update("won", true)
	}

	//match finished
	GetDB().Model(&match).Updates(map[string]interface{}{"finished": true, "finished_date": match.FinishedDate, "status": match.Status, "home_score": match.HomeScore, "away_score": match.AwayScore})

	resp := u.Message(true, "success")
	return resp
}

func (match *Match) CancelOrPostponeMatch() map[string]interface{} {
	GetDB().Model(&match).Updates(map[string]interface{}{"status": match.Status, "finished": true})
	GetDB().Delete(Bet{}, "match_id = ?", match.ID)
	GetDB().Delete(ChallengeMatch{}, "match_id = ?", match.ID)
	resp := u.Message(true, "success")
	resp["match"] = match
	return resp
}

func GetRankings(year uint, month uint, userid uint) *RankingReturn {
	ret := &RankingReturn{}
	userRet := &Ranking{}
	rankings := make([]Ranking, 0)

	//matches := make([]*ChallengeMatch,0)
	//err := GetDB().Model("challenges").Preload("Matches").Preload("Matches.Match").Preload("Matches.Match.Bets","user_id = COALESCE(NULLIF(?,0),user_id)",userid).Preload("Contenders","user_id = COALESCE(NULLIF(?,0),user_id)",userid).Find(&challenges).Error
	err := GetDB().Model("rankings").Limit(10).Order("rank").Where("year = ? and month = ?", year, month).Find(&rankings).Error
	if err != nil {
		return nil
	}
	if !GetDB().Model("rankings").Where("year = ? and month = ? and user_id = ?", year, month, userid).Find(&userRet).RecordNotFound() {
		ret.UserRanking = userRet.Rank
		ret.UserPoints = userRet.Points
	}
	ret.Rankings = rankings
	ret.Year = year
	ret.Month = month
	return ret
}
