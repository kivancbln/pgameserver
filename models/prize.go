package models

import (
	"fmt"
	u "predictiongame/utils"
	"time"

	"github.com/jinzhu/gorm"
)

type Prize struct {
	gorm.Model
	PrizeType       uint64      `json:"prizeType"` //1-points 2-money 3-customer coupon money 4-customer coupon percent 5-coin
	Prize           float32     `json:"prize"`
	TotalOrEach     uint64      `json:"totalOrEach"` //1 total 2 each
	CustomerId      uint64      `json:"customer"`    //
	ExpirationDate  time.Time   `json:"expirationDate"`
	PrizeTypeDesc   PrizeType   `gorm:"FOREIGNKEY:ID;ASSOCIATION_FOREIGNKEY:PrizeType"`
	TotalOrEachDesc TotalOrEach `gorm:"FOREIGNKEY:ID;ASSOCIATION_FOREIGNKEY:TotalOrEach"`
}

type PrizeType struct {
	gorm.Model
	Description string `json:"description"`
}

type TotalOrEach struct {
	gorm.Model
	Description string `json:"description"`
}

type Customer struct {
	gorm.Model
	CustomerName string `json:"customerName"`
}

type Buyable struct {
	gorm.Model
	CustomerID  uint64   `json:"customerId"`
	Description string   `json:"description"`
	Coin        float32  `json:"coin"`
	Active      bool     `json:"active" gorm:"default:false"`
	Customer    Customer `gorm:"FOREIGNKEY:ID;ASSOCIATION_FOREIGNKEY:CustomerID"`
}

// BuyOrder when users want to buy sth. it is created and inserted
type BuyOrder struct {
	gorm.Model
	BuyableID uint64  `json:"buyableId"`
	UserID    uint64  `json:"userId"`
	Email     string  `json:"email"`
	Processed bool    `json:"processed" gorm:"default:false"`
	Buyable   Buyable `gorm:"FOREIGNKEY:ID;ASSOCIATION_FOREIGNKEY:BuyableID"`
}

// Create to create prize
func (prize *Prize) Create() map[string]interface{} {

	//if resp, ok := prize.Validate(); !ok {
	//	return resp
	//}

	GetDB().Create(prize)

	resp := u.Message(true, "success")
	resp["prize"] = prize
	return resp
}

// GetBuyables to return active buyables
func GetBuyables() []*Buyable {

	buyables := make([]*Buyable, 0)
	err := GetDB().Model("buyables").Preload("Customer").Where("active = true").Find(&buyables).Error
	if err != nil {
		return nil
	}
	return buyables
}

// GetBuyOrders to return active buy orders
func GetBuyOrders(userid uint) []*BuyOrder {

	buyorders := make([]*BuyOrder, 0)
	err := GetDB().Model("buyorders").Preload("Buyable").Preload("Buyable.Customer").Where("active = true and user_id = ?", userid).Find(&buyorders).Error
	if err != nil {
		return nil
	}
	return buyorders
}

// CreateBuyOrder to create buy order
func (buyorder *BuyOrder) CreateBuyOrder() map[string]interface{} {

	GetDB().Create(buyorder)

	resp := u.Message(true, "success")
	resp["buyorder"] = buyorder
	return resp
}

// GetCoin to return coin of user
func GetCoin(userid uint) *int {
	var coin int
	var spent int

	fmt.Println("get coin")

	s := fmt.Sprintf(`select coalesce(sum(prize_won),0) total from winnings where user_id=%d`, userid)

	err := GetDB().Raw(s).Row().Scan(&coin)
	if err != nil {
		fmt.Println(err.Error())
		return nil
	}

	fmt.Println(coin)

	s2 := fmt.Sprintf(`select coalesce(sum(coin),0) total from buy_orders bo 
					  inner join buyables b on b.id = bo.buyable_id
					  where bo.user_id=%d`, userid)
	err2 := GetDB().Raw(s2).Row().Scan(&spent)
	if err2 != nil {
		fmt.Println(err2.Error())
		return nil
	}

	fmt.Println(spent)

	res := coin - spent

	fmt.Println(res)

	return &res
}
