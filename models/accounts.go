package models

import (
	"os"
	u "predictiongame/utils"

	//"strings"
	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

/*
JWT claims struct
*/
type Token struct {
	UserId uint
	exp    int64
	jwt.StandardClaims
}

//Account a struct to rep user account
type Account struct {
	gorm.Model
	Email              string         `json:"email"`
	Password           string         `json:"password"`
	Token              string         `json:"token";sql:"-"`
	LastLoginDate      time.Time      `json:"lastLoginDate"`
	Uuid               string         `json:"uuid"`
	Serial             string         `json:"serial"`
	DeviceModel        string         `json:"model"`
	Guest              bool           `json:"guest"`
	Merged             bool           `json:"merged"`
	UserName           string         `json:"userName"`
	SubscriptionId     string         `json:"subscriptionId"`
	LastWatchDate      time.Time      `json:"lastWatchDate"`
	ActualComboCount   uint           `json:"actualComboCount"`
	PreviousComboCount uint           `json:"previousComboCount"`
	GeneralSettingID   uint64         `json:"generalSettingId"  gorm:"default:1"`
	ReferenceUserID    uint           `gorm:"referenceUserId" gorm:"default:0"`
	Points             float32        `json:"points" gorm:"default:0"`
	GeneralSetting     GeneralSetting `gorm:"FOREIGNKEY:ID;ASSOCIATION_FOREIGNKEY:GeneralSettingID"`
}

// ConfirmOrCancelModel to confirm or cancel merge operation
type ConfirmOrCancelModel struct {
	Uuid            string `json:"uuid"`
	Userid          uint   `json:"userid"`
	Confirmorcancel uint   `json:"confirmorcancel"`
}

// AccountUserNameUpdate is used to update UserName
type AccountUserNameUpdate struct {
	UserName string `json:"userName"`
}

// AccountSubscriptionIdUpdate is used to update subscriptionId
type AccountSubscriptionIdUpdate struct {
	SubscriptionId string `json:"subscriptionId"`
}

// AccountReferenceUserIDUpdate is used to update ReferenceUserId
type AccountReferenceUserIDUpdate struct {
	ReferenceUserID uint `json:"referenceUserId"`
}

//Validate incoming user details...
func (account *Account) Validate() (map[string]interface{}, bool) {

	//if !strings.Contains(account.Email, "@") {
	//	return u.Message(false, "Email address is required"), false
	//}

	if len(account.Password) < 6 {
		return u.Message(false, "Password is required, minimum 6 characters."), false
	}

	//Email must be unique
	temp := &Account{}

	//check for errors and duplicate emails
	err := GetDB().Table("accounts").Where("email = ?", account.Email).First(temp).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return u.Message(false, "Connection error. Please retry"), false
	}
	if temp.Email != "" {
		return u.Message(false, "Email address already in use by another user."), false
	}

	return u.Message(false, "Requirement passed"), true
}

// Create is used to create account
func (account *Account) Create() map[string]interface{} {

	if resp, ok := account.Validate(); !ok {
		return resp
	}

	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(account.Password), bcrypt.DefaultCost)
	account.Password = string(hashedPassword)

	GetDB().Create(account)

	if account.ID <= 0 {
		return u.Message(false, "Failed to create account, connection error.")
	}

	//Create new JWT token for the newly registered account
	tk := &Token{UserId: account.ID}
	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
	tokenString, _ := token.SignedString([]byte(os.Getenv("token_password")))
	account.Token = tokenString

	account.Password = "" //delete password

	response := u.Message(true, "Account has been created")
	response["account"] = account
	return response
}

//Login is used to login
func Login(email, password string, uuid string, serial string, model string) map[string]interface{} {

	account := &Account{}
	err := GetDB().Table("accounts").Preload("GeneralSetting").Where("email = ?", email).First(account).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return u.Message(false, "Email address not found")
		}
		return u.Message(false, "Connection error. Please retry")
	}

	err = bcrypt.CompareHashAndPassword([]byte(account.Password), []byte(password))
	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword { //Password does not match!
		return u.Message(false, "Invalid login credentials. Please try again")
	}
	//Worked! Logged In
	account.Password = ""

	expirationTime := time.Now().Add(8 * time.Hour)

	//Create JWT token
	tk := &Token{UserId: account.ID,
		StandardClaims: jwt.StandardClaims{
			// In JWT, the expiry time is expressed as unix milliseconds
			ExpiresAt: expirationTime.Unix(),
		}}
	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
	tokenString, _ := token.SignedString([]byte(os.Getenv("token_password")))
	account.Token = tokenString //Store the token in the response

	db.Model(&account).Where("email = ?", email).Updates(map[string]interface{}{"last_login_date": time.Now(), "uuid": uuid, "device_model": model, "serial": serial})

	resp := u.Message(true, "Logged In")
	resp["account"] = account
	return resp
}

// GetUser returns user info
func GetUser(u uint) *Account {

	acc := &Account{}
	GetDB().Table("accounts").Where("id = ?", u).First(acc)
	if acc.Email == "" { //User not found!
		return nil
	}

	acc.Password = ""
	return acc
}

// GetUserByEmail returns user by email
func GetUserByEmail(email string) *Account {

	acc := &Account{}
	err := GetDB().Table("accounts").Where("email = ?", email).First(acc).Error
	if err != nil {
		return nil
	}

	return acc
}

// GetUserByUUID returns user by uuid
func GetUserByUUID(uuid string) *Account {

	acc := &Account{}
	err := GetDB().Table("accounts").Where("email = ? and merged = false", uuid).First(acc).Error
	if err != nil {
		return nil
	}

	return acc
}

// ConfirmOrCancel does the merge operation
func ConfirmOrCancel(uuid string, userid uint, confirmorcancel uint) map[string]interface{} {

	accounts := make([]*Account, 0)
	s := fmt.Sprintf(`DO $$
	declare new_id int:= %d;
			_uuid varchar(100):='%s';
			 confirmorcancel int:=%d;
			old_id int:=0;
	begin
		select id into old_id from accounts where email = _uuid and merged = false;
		if(confirmorcancel = 1) then
			update bets set user_id = new_id where user_id = old_id;
			update challenge_contenders set user_id = new_id where user_id = old_id;
			update gains set user_id = new_id where user_id = old_id;
			update winnings set user_id = new_id where user_id = old_id;
			update transactions set user_id = new_id where user_id = old_id; 
		end if;
		update accounts set deleted_at = now(), merged = true where email = _uuid;
	END $$;
	select * from accounts where id = %d;
	`, userid, uuid, confirmorcancel, userid)

	err := GetDB().Raw(s).Scan(&accounts).Error
	if err != nil {
		resp := u.Message(false, err.Error())
		return resp
	}
	resp := u.Message(true, "success")
	resp["data"] = accounts
	return resp
}

// UpdateUsername updates username
func UpdateUsername(userid uint, username string) map[string]interface{} {

	GetDB().Table("accounts").Where("id = ?", userid).Updates(map[string]interface{}{"user_name": username})

	return u.Message(true, "success")
}

// UpdateSubscriptionId updates subscriptionId
func UpdateSubscriptionId(userid uint, subscriptionid string) map[string]interface{} {

	GetDB().Table("accounts").Where("id = ?", userid).Updates(map[string]interface{}{"subscription_id": subscriptionid})

	return u.Message(true, "success")
}

// UpdateReferenceID updates referenceId and create gain
func UpdateReferenceID(userid uint, referenceID uint) map[string]interface{} {

	GetDB().Table("accounts").Where("id = ?", userid).Updates(map[string]interface{}{"reference_user_id": referenceID})

	gain := &Gain{}
	gain.UserId = uint64(referenceID)
	gain.Type = 5
	gain.GainedPoints = 100
	GetDB().Create(gain)

	return u.Message(true, "success")
}

// UpdateLastLoginDate updates last login date
func UpdateLastLoginDate(userid uint) map[string]interface{} {

	accounts := make([]*Account, 0)

	s := fmt.Sprintf(`update accounts set last_login_date = now() where id=%d; select * from accounts where id = %d;`, userid, userid)
	err := GetDB().Raw(s).Scan(&accounts).Error
	if err != nil {
		resp := u.Message(false, err.Error())
		return resp
	}
	return u.Message(true, "success")
}

// PlusOrMinusPoints to update points in accounts
func PlusOrMinusPoints(userid uint64, points float32) map[string]interface{} {
	accounts := make([]*Account, 0)
	s := fmt.Sprintf(`update accounts set points = coalesce(points,0) + %f where id = %d;`, points, userid)
	fmt.Println(s)
	err := GetDB().Raw(s).Scan(&accounts).Error
	if err != nil {
		resp := u.Message(false, err.Error())
		return resp
	}
	return u.Message(true, "success")
}
