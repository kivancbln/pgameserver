package models

import (
	"github.com/jinzhu/gorm"
	u "predictiongame/utils"
)

type Winning struct {
	gorm.Model
	ChallengeId   uint64 `json:"challengeId"` //1-points 2-money 3-customer coupon money 4-customer coupon percent
	UserId  uint64 `json:"userId"`
	PrizeId uint64   `json:"prizeId"` //1 total 2 each
	PrizeWon float32 `json:"prizeWon"` //
}

func (winning *Winning) Create() (map[string]interface{}) {

	//if resp, ok := prize.Validate(); !ok {
	//	return resp
	//}

	GetDB().Create(winning)

	resp := u.Message(true, "success")
	resp["winning"] = winning
	return resp
}