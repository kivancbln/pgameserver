package models

import (

	//"strings"

	"github.com/jinzhu/gorm"
)

type GeneralSetting struct {
	gorm.Model
	MarketActive bool   `json:"marketActive"`
	Version      string `json:"version"`
}
