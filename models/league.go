package models

import (
	"github.com/jinzhu/gorm"
	u "predictiongame/utils"
	"time"
)

type League struct {
	gorm.Model
	LeagueId   uint64 `json:"leagueId" mapstructure:"league_id"`
	Name  string `json:"name"`
	Type string `json:"type"`
	Country string `json:"country"`
	CountryCode string `json:"countryCode" mapstructure:"country_code"`
	Season float64 `json:"season"`
	SeasonStart time.Time `json:"seasonStart" mapstructure:"season_start"`
	SeasonEnd time.Time `json:"seasonEnd" mapstructure:"season_end"`
	Logo string `json:"logo"`
	Flag string `json:"flag"`
	Priority uint `json:"priority"`
}

func (league *League) Create() (map[string]interface{}) {

	GetDB().Create(league)

	resp := u.Message(true, "success")
	resp["league"] = league
	return resp
}

func GetLeagues() ([]*League) {

	leagues := make([]*League, 0)
	err := GetDB().Model("leagues").Find(&leagues).Error
	if err != nil {
		return nil
	}
	return leagues
}