package models


import (
	u "predictiongame/utils"
	//"strings"
	"github.com/jinzhu/gorm"
)


type ActionLog struct {
	gorm.Model
	UserId    uint `json:"userid"`
	Action string `json:"action"`
}


func (actionlog *ActionLog) Create() map[string]interface{} {

	GetDB().Create(actionlog)

	response := u.Message(true, "Action logged")
	response["log"] = actionlog
	return response
}