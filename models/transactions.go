package models

import (
	"github.com/jinzhu/gorm"
	u "predictiongame/utils"
)

type Transaction struct {
	gorm.Model
	UserId uint `json:"userId"`
	TransactionId   string `json:"transactionId"` 
	Receipt  string `json:"receipt"`
	Signature string `json:"signature"`
	ProductType string `json:"productType"`
}

func (transaction *Transaction) Create() (map[string]interface{}) {

	//if resp, ok := prize.Validate(); !ok {
	//	return resp
	//}

	GetDB().Create(transaction)

	resp := u.Message(true, "success")
	resp["data"] = transaction
	return resp
}