package models

import (
	"fmt"
	u "predictiongame/utils"
	"time"

	"github.com/jinzhu/gorm"
	"github.com/patrickmn/go-cache"
)

type Challenge struct {
	gorm.Model
	Name          string    `json:"name"`
	StartDate     time.Time `json:"startDate"`
	EndDate       time.Time `json:"endDate"`
	MinPoints     uint64    `json:"minPoints"`
	PrizeId       uint64    `json:"prizeId"`
	Finished      bool      `json:"finished" gorm:"default:false"`
	PointsToSpend uint64    `json:"pointsToSpend"`
	MinContender  uint64    `json:"minContender"`
	Type          string    `json:"type"`
	Priority      uint      `json:"priority" gorm:"default:0"`
	Matches       []ChallengeMatch
	Contenders    []ChallengeContender
	Prize         Prize `gorm:"FOREIGNKEY:ID;ASSOCIATION_FOREIGNKEY:PrizeId"`
	Results       []Result
}

type ChallengeMatch struct {
	gorm.Model
	ChallengeId uint64 `json:"challengeId"`
	MatchId     uint64 `json:"matchId"`
	Match       Match  `gorm:"FOREIGNKEY:ID;ASSOCIATION_FOREIGNKEY:MatchId"`
}

type ChallengeContender struct {
	gorm.Model
	ChallengeId uint64  `json:"challengeId"`
	UserId      uint64  `json:"userId"`
	Completed   bool    `json:"completed" gorm:"default:false"`
	PointsWon   float32 `json:"pointsWon"`
	IsWinner    bool    `json:"isWinner" gorm:"default:false"`
}

type ChallengeMatchDetail struct {
	*Match
}

type Result struct {
	ChallengeId uint64  `json:"challengeId"`
	Name        string  `json:"name"`
	Points      float32 `json:"points"`
	Rank        uint64  `json:"rank"`
}

func (challenge *Challenge) CreateChallenge() map[string]interface{} {

	//if resp, ok := gain.Validate(); !ok {
	//	return resp
	//}

	GetDB().Create(challenge)

	resp := u.Message(true, "success")
	resp["challenge"] = challenge
	return resp
}

func GetChallenges(userid uint64) []*Challenge {

	challenges := make([]*Challenge, 0)
	//matches := make([]*ChallengeMatch,0)
	//err := GetDB().Model("challenges").Preload("Matches").Preload("Matches.Match").Preload("Matches.Match.Bets","user_id = COALESCE(NULLIF(?,0),user_id)",userid).Preload("Contenders","user_id = COALESCE(NULLIF(?,0),user_id)",userid).Find(&challenges).Error
	err := GetDB().Model("challenges").Preload("Prize").Preload("Prize.PrizeTypeDesc").Preload("Prize.TotalOrEachDesc").Preload("Matches").Preload("Matches.Match").Preload("Matches.Match.League").Preload("Contenders", "user_id = COALESCE(NULLIF(?,0),user_id)", userid).Order("priority desc").Find(&challenges).Error
	for i, _ := range challenges {
		for j, _ := range challenges[i].Matches {

			db.Model(challenges[i].Matches[j].Match).Find(&challenges[i].Matches[j].Match.Bets, "challenge_id = ? and user_id = ? and match_id = ?", challenges[i].ID, userid, challenges[i].Matches[j].Match.ID)
		}
	}
	if err != nil {
		return nil
	}
	return challenges
}

func GetActiveChallenges(userid uint64) []*Challenge {

	challenges := make([]*Challenge, 0)
	//matches := make([]*ChallengeMatch,0)
	//err := GetDB().Model("challenges").Preload("Matches").Preload("Matches.Match").Preload("Matches.Match.Bets","user_id = COALESCE(NULLIF(?,0),user_id)",userid).Preload("Contenders","user_id = COALESCE(NULLIF(?,0),user_id)",userid).Find(&challenges).Error
	err := GetDB().Model("challenges").Preload("Prize").Preload("Prize.PrizeTypeDesc").Preload("Prize.TotalOrEachDesc").Preload("Matches").Preload("Matches.Match").Preload("Matches.Match.MatchOdds").Preload("Matches.Match.League").Preload("Contenders", "user_id = COALESCE(NULLIF(?,0),user_id)", userid).Where("finished = false and start_date>=CURRENT_DATE and start_date<CURRENT_DATE + INTERVAL '1 day'").Order("priority desc").Find(&challenges).Error
	for i, _ := range challenges {
		for j, _ := range challenges[i].Matches {

			db.Model(challenges[i].Matches[j].Match).Find(&challenges[i].Matches[j].Match.Bets, "challenge_id = ? and user_id = ? and match_id = ?", challenges[i].ID, userid, challenges[i].Matches[j].Match.ID)
			for k, _ := range challenges[i].Matches[j].Match.Bets {
				db.Model(challenges[i].Matches[j].Match.Bets[k]).Find(&challenges[i].Matches[j].Match.Bets[k].Odd, "id = ?", challenges[i].Matches[j].Match.Bets[k].OddId)
			}
		}
	}
	if err != nil {
		return nil
	}
	return challenges
}

func GetActiveChallengesPaged(userid uint64, page uint64, offset uint64) []*Challenge {

	challenges := make([]*Challenge, 0)
	err := GetDB().Model("challenges").Preload("Prize").Preload("Prize.PrizeTypeDesc").Preload("Prize.TotalOrEachDesc").Preload("Matches").Preload("Matches.Match").Preload("Matches.Match.MatchOdds").Preload("Matches.Match.League").Preload("Contenders", "user_id = COALESCE(NULLIF(?,0),user_id)", userid).Where("finished = false and start_date>=CURRENT_DATE and start_date<CURRENT_DATE + INTERVAL '1 day'").Order("priority desc").Limit(offset).Offset((page - 1) * offset).Find(&challenges).Error
	for i, _ := range challenges {
		for j, _ := range challenges[i].Matches {
			db.Model(challenges[i].Matches[j].Match).Find(&challenges[i].Matches[j].Match.Bets, "challenge_id = ? and user_id = ? and match_id = ?", challenges[i].ID, userid, challenges[i].Matches[j].Match.ID)
			for k, _ := range challenges[i].Matches[j].Match.Bets {
				db.Model(challenges[i].Matches[j].Match.Bets[k]).Find(&challenges[i].Matches[j].Match.Bets[k].Odd, "id = ?", challenges[i].Matches[j].Match.Bets[k].OddId)
			}
		}
	}
	if err != nil {
		return nil
	}
	return challenges
}

func GetActiveChallengesFromCache(userid uint64) []*Challenge {

	challenges := make([]*Challenge, 0)
	// matches := make([]*ChallengeMatch,0)
	// err := GetDB().Model("challenges").Preload("Matches").Preload("Matches.Match").Preload("Matches.Match.Bets","user_id = COALESCE(NULLIF(?,0),user_id)",userid).Preload("Contenders","user_id = COALESCE(NULLIF(?,0),user_id)",userid).Find(&challenges).Error
	// err := GetDB().Model("challenges").Preload("Prize").Preload("Prize.PrizeTypeDesc").Preload("Prize.TotalOrEachDesc").Preload("Matches").Preload("Matches.Match").Preload("Matches.Match.MatchOdds").Preload("Matches.Match.League").Where("finished = false and start_date>=CURRENT_DATE and start_date<CURRENT_DATE + INTERVAL '1 day'").Order("priority desc").Find(&challenges).Error

	if x, found := u.GetCache().Get("todaysChallenges"); found {
		challenges = x.([]*Challenge)
		fmt.Println("found")
	}
	for i, _ := range challenges {
		db.Model(challenges[i]).Find(&challenges[i].Contenders, "challenge_id = ?  and user_id = COALESCE(NULLIF(?,0),user_id)", challenges[i].ID, userid)
		for j, _ := range challenges[i].Matches {
			db.Model(challenges[i].Matches[j].Match).Find(&challenges[i].Matches[j].Match.Bets, "challenge_id = ? and user_id = ? and match_id = ?", challenges[i].ID, userid, challenges[i].Matches[j].Match.ID)
			for k, _ := range challenges[i].Matches[j].Match.Bets {
				db.Model(challenges[i].Matches[j].Match.Bets[k]).Find(&challenges[i].Matches[j].Match.Bets[k].Odd, "id = ?", challenges[i].Matches[j].Match.Bets[k].OddId)
			}
		}
	}

	return challenges
}

func GetFinishedChallenges(userid uint64) []*Challenge {

	challenges := make([]*Challenge, 0)
	//matches := make([]*ChallengeMatch,0)
	//err := GetDB().Model("challenges").Preload("Matches").Preload("Matches.Match").Preload("Matches.Match.Bets","user_id = COALESCE(NULLIF(?,0),user_id)",userid).Preload("Contenders","user_id = COALESCE(NULLIF(?,0),user_id)",userid).Find(&challenges).Error
	err := GetDB().Model("challenges").Preload("Prize").Preload("Prize.PrizeTypeDesc").Preload("Prize.TotalOrEachDesc").Preload("Matches").Preload("Matches.Match").Preload("Matches.Match.League").Preload("Contenders", "user_id = COALESCE(NULLIF(?,0),user_id)", userid).Where("finished = true and start_date>=CURRENT_DATE - INTERVAL '1 day' and start_date<CURRENT_DATE").Find(&challenges).Error
	for i, _ := range challenges {
		for j, _ := range challenges[i].Matches {

			db.Model(challenges[i].Matches[j].Match).Find(&challenges[i].Matches[j].Match.Bets, "challenge_id = ? and user_id = ? and match_id = ?", challenges[i].ID, userid, challenges[i].Matches[j].Match.ID)
		}
	}
	if err != nil {
		return nil
	}
	return challenges
}

func GetFinishedChallengesPaged(userid uint64, page uint64, offset uint64) []*Challenge {

	challenges := make([]*Challenge, 0)
	//matches := make([]*ChallengeMatch,0)
	//err := GetDB().Model("challenges").Preload("Matches").Preload("Matches.Match").Preload("Matches.Match.Bets","user_id = COALESCE(NULLIF(?,0),user_id)",userid).Preload("Contenders","user_id = COALESCE(NULLIF(?,0),user_id)",userid).Find(&challenges).Error
	err := GetDB().Model("challenges").Preload("Prize").Preload("Prize.PrizeTypeDesc").Preload("Prize.TotalOrEachDesc").Preload("Matches").Preload("Matches.Match").Preload("Matches.Match.League").Preload("Contenders", "user_id = COALESCE(NULLIF(?,0),user_id)", userid).Where("finished = true and start_date>=CURRENT_DATE - INTERVAL '1 day' and start_date<CURRENT_DATE").Limit(offset).Offset((page - 1) * offset).Find(&challenges).Error
	for i, _ := range challenges {
		for j, _ := range challenges[i].Matches {

			db.Model(challenges[i].Matches[j].Match).Find(&challenges[i].Matches[j].Match.Bets, "challenge_id = ? and user_id = ? and match_id = ?", challenges[i].ID, userid, challenges[i].Matches[j].Match.ID)
		}
	}
	if err != nil {
		return nil
	}
	return challenges
}

func GetResultsByChallenge(challengeid uint64) []*Result {

	results := make([]*Result, 0)
	//matches := make([]*ChallengeMatch,0)
	//err := GetDB().Model("challenges").Preload("Matches").Preload("Matches.Match").Preload("Matches.Match.Bets","user_id = COALESCE(NULLIF(?,0),user_id)",userid).Preload("Contenders","user_id = COALESCE(NULLIF(?,0),user_id)",userid).Find(&challenges).Error
	s := fmt.Sprintf(`select challenge_id,a.email as name
	,points
	,rank() over(order by points desc) as rank 
	from (select c.ID as challenge_id,g.user_id,max(prize_id) prize_id,sum(g.gained_points) points 
from gains g
inner join challenge_matches cm on g.match_id = cm.match_id and g.challenge_id = cm.challenge_id
inner join challenge_contenders cc on cc.user_id = g.user_id and cc.challenge_id = cm.challenge_id
inner join challenges c on c.ID = cm.challenge_id
where c.ID = %d and cc.completed = true
group by c.ID,g.user_id) zz
inner join accounts a on a.ID = zz.user_id
order by rank;
`, challengeid)

	err := GetDB().Raw(s).Find(&results).Error
	if err != nil {
		return nil
	}
	return results
}

func GetChallengeById(userid uint64, challengeId uint64) []*Challenge {

	challenges := make([]*Challenge, 0)
	//matches := make([]*ChallengeMatch,0)
	err := GetDB().Model("challenges").Preload("Prize").Preload("Prize.PrizeTypeDesc").Preload("Prize.TotalOrEachDesc").Preload("Matches").Preload("Matches.Match").Preload("Matches.Match.MatchOdds").Preload("Contenders", "user_id = COALESCE(NULLIF(?,0),user_id)", userid).Where("finished = false and ID = ?", challengeId).Find(&challenges).Error
	for i, _ := range challenges {
		for j, _ := range challenges[i].Matches {

			db.Model(challenges[i].Matches[j].Match).Find(&challenges[i].Matches[j].Match.Bets, "challenge_id = ? and user_id = ? and match_id = ?", challenges[i].ID, userid, challenges[i].Matches[j].Match.ID)
		}
	}
	//err := GetDB().Model("challenges").Where("finished = false and ID = ?",challengeId).Preload("Matches").Preload("Matches.Match").Preload("Matches.Match.Bets").Preload("Matches.Bets","user_id = COALESCE(NULLIF(?,0),user_id)",userid).Preload("Contenders","user_id = COALESCE(NULLIF(?,0),user_id)",userid).Find(&challenges).Error
	if err != nil {
		return nil
	}
	return challenges
}

func (match *ChallengeMatch) CreateChallengeMatch() map[string]interface{} {

	//if resp, ok := gain.Validate(); !ok {
	//	return resp
	//}

	err := GetDB().Create(match).Error
	if err != nil {
		resp := u.Message(false, err.Error())
		return resp
	}
	resp := u.Message(true, "success")
	resp["matches"] = match
	return resp
}

func (contender *ChallengeContender) CreateChallengeContender() map[string]interface{} {

	//if resp, ok := gain.Validate(); !ok {
	//	return resp
	//}

	err := GetDB().Create(contender).Error
	if err != nil {
		resp := u.Message(false, err.Error())
		return resp
	}
	var points float32
	s := fmt.Sprintf(`select min_points from challenges c where c.id=%d`, contender.ChallengeId)
	GetDB().Raw(s).Select("points").Row().Scan(&points)
	PlusOrMinusPoints(contender.UserId, -1.0*points)
	resp := u.Message(true, "success")
	resp["contender"] = contender
	return resp
}

func CheckChallengeCompleted(userid uint64, challengeid uint64) map[string]interface{} {

	s := fmt.Sprintf(`update challenge_contenders set completed = cnt
	from challenge_contenders cc
	inner join (
	select %d as user_id,c.id,case when sum(case when b.id is null then 1 else 0 end) = 0 then true else false end  cnt from challenges c
	inner join challenge_matches cm on cm.challenge_id = c.id
	left join bets b on b.challenge_id  = c.id 
						and b.match_id = cm.match_id 
						and b.deleted_at is null 
						and b.user_id = %d
	where c.id = %d
	group by c.id) uc on cc.user_id = uc.user_id and cc.challenge_id = uc.id;`, userid, userid, challengeid)
	err := GetDB().Raw(s).Error
	if err != nil {
		resp := u.Message(false, err.Error())
		return resp
	}
	resp := u.Message(true, "success")
	return resp
}

func FinishChallenge(challengeid uint) map[string]interface{} {

	challenges := make([]*Challenge, 0)
	s := fmt.Sprintf(`DROP TABLE IF EXISTS _gains;
	create temp table _gains(challenge_id int,user_id int,prize_id int,points float);
	DROP TABLE IF EXISTS _winners;
	create temp table _winners(challenge_id int,user_id int,prize_id int,rank int);
	
	
	DO $$
	declare v_challengeid int:= %d;
	 v_matchcount int:=0;
	 v_finishedcount int:=0;
	 v_winnercount int:=0;
	 v_winningprize float;
	 v_prizetype int;
	 v_finished bool;
	 v_finishedcontender int:=0;
	 v_mincontender int:=0;
	begin
		select finished into v_finished from challenges where ID = v_challengeid;
		if( v_finished = false) then
			select sum(1), sum(case when m.finished = true then 1 else 0 end) 
			into v_matchcount, v_finishedcount
			from challenge_matches cm
			inner join matches m on m.ID = cm.match_id
			inner join challenges c on cm.challenge_id = c.ID
			where c.ID = v_challengeid
			and cm.deleted_at IS NULL
			and m.deleted_at IS NULL
			and c.deleted_at IS NULL
			group by c.ID;

			if(COALESCE(v_matchcount,0) = 0) then
				update challenge_contenders set deleted_at = now() where challenge_id = v_challengeid;
				update challenges set finished = true where ID = v_challengeid;
			else
				update challenge_contenders set completed = cnt
				from challenge_contenders cc
				inner join (
				select b.user_id,c.id,case when sum(case when b.id is null then 1 else 0 end) = 0 then true else false end  cnt from challenges c
				inner join challenge_matches cm on cm.challenge_id = c.id
				left join bets b on b.challenge_id  = c.id 
									and b.match_id = cm.match_id 
									and b.deleted_at is null 
				where c.id = v_challengeid
				and cm.deleted_at IS NULL
				group by b.user_id,c.id) uc on cc.user_id = uc.user_id and cc.challenge_id = uc.id;
		
				if(COALESCE(v_matchcount,0) = COALESCE(v_finishedcount,0)) then

					select sum(1),max(COALESCE(c.min_contender,1))
					into v_finishedcontender,v_mincontender
					from challenge_contenders cc
					inner join challenges c on cc.challenge_id = c.ID
					where c.ID = v_challengeid
					and cc.deleted_at IS NULL
					and c.deleted_at IS NULL
					and cc.completed = true
					group by c.ID;
					
					if(COALESCE(v_finishedcontender,0) >= COALESCE(v_mincontender,0)) then

						insert into _gains
						select c.ID as challenge_id,g.user_id,max(prize_id) prize_id,sum(g.gained_points) points 
						from gains g
						inner join challenge_matches cm on g.match_id = cm.match_id and g.challenge_id = cm.challenge_id
						inner join challenge_contenders cc on cc.user_id = g.user_id and cc.challenge_id = cm.challenge_id
						inner join challenges c on c.ID = cm.challenge_id
						where c.ID = v_challengeid and cc.completed = true
						and cm.deleted_at IS NULL
						and cc.deleted_at IS NULL
						and c.deleted_at IS NULL
						group by c.ID,g.user_id;	
			
						insert into _winners
						select * 
						from (select challenge_id,user_id
							,prize_id
							,rank() over(order by points desc) rank_number 
							from _gains) xx
						where xx.rank_number = 1;
			
						select count(1) into v_winnercount from _winners;

						if (COALESCE(v_winnercount,0) >0) then
							select case when p.total_or_each = 1 then p.prize else p.prize/v_winnercount end,
							p.prize_type into v_winningprize, v_prizetype
							from prizes p
							inner join challenges c on c.prize_id = p.ID
							where c.ID = v_challengeid;
				
							if(v_prizetype = 1) then
								INSERT INTO public.gains(
								created_at, updated_at, user_id, match_id, type, gained_points, challenge_id)
								select now(),now(),user_id,0,3,v_winningprize,v_challengeid
								from _winners;

								update accounts a set points = points + v_winningprize
								from  _winners w where w.user_id = a.id;
							end if;
				
							if(v_prizetype in (2,3,4,5)) then
								INSERT INTO public.winnings(
								created_at, updated_at, challenge_id, user_id, prize_id, prize_won)
								select now(),now(),v_challengeid,user_id,prize_id,v_winningprize
								from _winners;
							end if;
				
							update challenge_contenders cc set is_winner = true, points_won = v_winningprize 
							from  _winners w where w.user_id = cc.user_id 
							and w.challenge_id = cc.challenge_id;
				
							update challenges set finished = true where ID = v_challengeid;
							
							insert into gains(
							created_at, updated_at, user_id, match_id, type, gained_points, challenge_id)
							select now(),now(),cc.user_id,0,4,min_points,c.ID from challenges c
							inner join challenge_contenders cc on cc.challenge_id = c.ID 
							where c.ID = v_challengeid and cc.completed = false;

							update accounts a set points = points + c.min_points
							from challenge_contenders cc 
							inner join challenges c on c.ID = cc.challenge_id
							where c.ID = v_challengeid and cc.completed = false
							and cc.user_id = a.id;
						else
							update challenges set finished = true where ID = v_challengeid;
						end if;
					else 
						update challenge_contenders set deleted_at = now() where challenge_id = v_challengeid;
						update challenges set finished = true where ID = v_challengeid;
					end if;
				end if;
			end if;
		end if;
	END $$;
	
	select * from challenges where ID = %d;
	
	`, challengeid, challengeid)

	err := GetDB().Raw(s).Scan(&challenges).Error
	if err != nil {
		resp := u.Message(false, err.Error())
		return resp
	}
	resp := u.Message(true, "success")
	resp["data"] = challenges
	return resp
}

func FinishActiveChallenges() []*Challenge {

	challenges := make([]*Challenge, 0)
	err := GetDB().Model("challenges").Where("finished = false").Find(&challenges).Error
	for i, _ := range challenges {
		fmt.Println(challenges[i].ID)
		FinishChallenge(challenges[i].ID)
	}
	if err != nil {
		return nil
	}
	return challenges
}

func AutoCreateChallenges(date1 string, date2 string) map[string]interface{} {
	challenges := make([]*Challenge, 0)

	s := fmt.Sprintf(`DROP TABLE IF EXISTS _matches;
	create temp table _matches(rnk int,lgid int,maid int,name text);
	DROP TABLE IF EXISTS _challenges;
	create temp table _challenges(rowid int,created_at timestamp,name text,startdate date,
								  enddate date,minpoints float,prizeid int,finished bool,
								  pointstospend float,mincontender int,rnk int);
	
	
	DO $$
		declare _startdate DATE:='%s';
			 _enddate DATE:='%s';
			 _insertedid int;
			 t_row record;
		begin
		insert into _matches
		select dense_rank() over( order by matches.league_id) rnk,
		matches.league_id,
		matches.id,
		leagues.name ChName
		from matches 
		inner join leagues on leagues.league_id = matches.league_id
		where date >=_startdate and date <_enddate
		and exists (select 1 from match_odds where fixture_id = matches.source_id);
	
	
		insert into _challenges
		select row_number() over(order by rnk) rowid, now(),
		name,_startdate,_enddate,20,1,false,100,0,rnk 
		from _matches group by rnk,lgid,name;
		
		FOR t_row in SELECT rowid FROM _challenges LOOP
			WITH rows AS (
			insert into challenges 
				(created_at,name,start_date,end_date,min_points,prize_id,finished,points_to_spend,min_contender,type)
			select 
				created_at,name,startdate,enddate,minpoints,prizeid,finished,pointstospend,mincontender,'Match Winner'
				from _challenges where rowid = t_row.rowid
				returning id
				)
			select id into _insertedid from rows;
			insert into challenge_matches
			(created_at,challenge_id,match_id)
			select now(),_insertedid,m.maid from _matches m
			inner join _challenges c on c.rnk = m.rnk
			where rowid=t_row.rowid ;
		END LOOP;
	END $$;
	select  * from challenges limit 1;
	`, date1, date2)

	err := GetDB().Raw(s).Scan(&challenges).Error
	if err != nil {
		resp := u.Message(false, err.Error())
		return resp
	}

	SetActiveChallengesCache()

	resp := u.Message(true, "success")
	resp["data"] = challenges
	return resp
}

func SetActiveChallengesCache() map[string]interface{} {
	// setting it to cache
	todaysChallenges := make([]*Challenge, 0)
	err2 := GetDB().Model("challenges").Preload("Prize").Preload("Prize.PrizeTypeDesc").Preload("Prize.TotalOrEachDesc").Preload("Matches").Preload("Matches.Match").Preload("Matches.Match.MatchOdds").Preload("Matches.Match.League").Where("finished = false and start_date>=CURRENT_DATE and start_date<CURRENT_DATE + INTERVAL '1 day'").Order("priority desc").Find(&todaysChallenges).Error
	if err2 != nil {
		resp := u.Message(false, err2.Error())
		return resp
	}

	u.GetCache().Set("todaysChallenges", todaysChallenges, cache.DefaultExpiration)
	if x, found := u.GetCache().Get("todaysChallenges"); found {
		todaysChallenges = x.([]*Challenge)
		fmt.Println("found after set")
	}
	resp := u.Message(true, "success")

	return resp
}
