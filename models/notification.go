package models

import (
	"github.com/jinzhu/gorm"
	u "predictiongame/utils"
	"time"
)

type Notification struct {
	gorm.Model
	Message string `json:"message"`
	Active bool `json:"active" gorm:"default:true"`
	ActiveTill time.Time `json:"activeTill"`
	NotificationRead NotificationRead `gorm:"FOREIGNKEY:NotificationId;ASSOCIATION_FOREIGNKEY:ID"`
}

type NotificationRead struct {
	gorm.Model
	NotificationId uint `json:"notificationId"` 
	UserId uint `json:"userId"`
}

func (notification *Notification) CreateNotification() (map[string]interface{}) {

	GetDB().Create(notification)

	resp := u.Message(true, "success")
	resp["notification"] = notification
	return resp
}

func (read *NotificationRead) ReadNotification() (map[string]interface{}) {

	GetDB().Create(read)

	resp := u.Message(true, "success")
	resp["read"] = read
	return resp
}


func GetNotifications(userid uint) ([]*Notification) {

	notifications := make([]*Notification, 0)
	//matches := make([]*ChallengeMatch,0)
	//err := GetDB().Model("challenges").Preload("Matches").Preload("Matches.Match").Preload("Matches.Match.Bets","user_id = COALESCE(NULLIF(?,0),user_id)",userid).Preload("Contenders","user_id = COALESCE(NULLIF(?,0),user_id)",userid).Find(&challenges).Error
	err := GetDB().Model("notifications").Preload("NotificationRead","user_id = COALESCE(NULLIF(?,0),user_id)",userid).Where("active_till > now()").Find(&notifications).Error
	
	if err != nil {
		return nil
	}
	return notifications
}