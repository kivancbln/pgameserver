package models

import (
	"fmt"
	u "predictiongame/utils"

	"github.com/jinzhu/gorm"
)

type Gain struct {
	gorm.Model
	UserId       uint64  `json:"userId"`
	MatchId      uint64  `json:"matchId"`
	ChallengeId  uint64  `json:"challengeId"`
	BetId        uint    `json:"betId"`
	Type         uint64  `json:"type"` //1-bet won 2-load 3-prize won 4-rewarded video
	GainedPoints float32 `json:"gainedPoints"`
}

type GainWithTeams struct {
	gorm.Model
	UserId       uint64  `json:"userId"`
	MatchId      uint64  `json:"matchId"`
	ChallengeId  uint64  `json:"challengeId"`
	Type         uint64  `json:"type"`
	GainedPoints float32 `json:"gainedPoints"`
	Home         string  `json:"home"`
	Away         string  `json:"away"`
}

type Transactions struct {
	gorm.Model
	UserId  uint64  `json:"userId"`
	MatchId uint64  `json:"matchId"`
	Type    int64   `json:"type"`
	Points  float32 `json:"points"`
	Home    string  `json:"home"`
	Away    string  `json:"away"`
}

func (gain *Gain) Validate() (map[string]interface{}, bool) {

	if gain.UserId == 0 {
		return u.Message(false, "UserId should be filled"), false
	}

	if gain.Type == 0 {
		return u.Message(false, "Type should be filled"), false
	}

	if gain.GainedPoints == 0 {
		return u.Message(false, "Gained Points should be filled"), false
	}

	if gain.Type == 1 && gain.MatchId == 0 {
		return u.Message(false, "MatchId should be filled when Type = 1"), false
	}

	//All the required parameters are present
	return u.Message(true, "success"), true
}

func (gain *Gain) CreateGain() map[string]interface{} {

	if resp, ok := gain.Validate(); !ok {
		return resp
	}

	if gain.Type == 4 {
		var points uint
		s := fmt.Sprintf(`DROP TABLE IF EXISTS _gains;
			create temp table _gains(points int);

		DO $$
			declare _combocount int := 0;
			declare _previouscount int := 0;
			declare _currentcount int := 0;
			declare _gain int := 0;
			begin
				select cast(DATE_PART('day', now() - COALESCE(last_watch_date,now()) ) as int)
				,COALESCE(actual_combo_count,0),COALESCE(previous_combo_count,0) 
				into _combocount
				, _currentcount, _previouscount 
				from accounts where id = %d;
				if (_combocount = 0) then
					select 1 into _gain;
				ELSIF  (_combocount = 1) then
					select _currentcount into _previouscount;
					select _previouscount+1 into _currentcount;
					select 2^_currentcount into _gain;
				ELSIF  (_combocount > 1) then
					select _currentcount into _previouscount;
					select 0 into _currentcount;
					select 2^_currentcount into _gain;
				end if;
				update accounts set updated_at=now(),last_watch_date=now(),actual_combo_count=_currentcount,previous_combo_count=_previouscount
				where id = %d;
				insert into _gains select _gain;
		END $$;

		select * from _gains;`, gain.UserId, gain.UserId)
		GetDB().Raw(s).Select("points").Row().Scan(&points)

		gain.GainedPoints = float32(points)

	}

	GetDB().Create(gain)
	PlusOrMinusPoints(gain.UserId, gain.GainedPoints)
	resp := u.Message(true, "success")
	resp["gain"] = gain
	return resp
}

// func (gain *Gain) CreateRewardedGain() (map[string]interface{}) {

// 	if resp, ok := gain.Validate(); !ok {
// 		return resp
// 	}

// 	s := fmt.Sprintf(`select * from accounts a `)

// 	GetDB().Create(gain)

// 	resp := u.Message(true, "success")
// 	resp["gain"] = gain
// 	return resp
// }

func GetPoints(userid uint64) []*GainWithTeams {

	gains := make([]*GainWithTeams, 0)
	err := GetDB().Table("gains").Select("gains.*,matches.home,matches.away").Joins("left join matches on gains.match_id = matches.ID").Where("gains.user_id = ?", userid).Find(&gains).Error
	if err != nil {
		return nil
	}
	return gains
}

func GetTransactions(userid uint64, page uint64, offset uint64) []*Transactions {
	transactions := make([]*Transactions, 0)
	s := fmt.Sprintf(`SELECT g.created_at, g.user_id, g.match_id, g.type, g.gained_points points,
	coalesce(m.home,'Points Load') as home,m.away
	FROM public.gains g
	left join public.matches m on g.match_id = m.ID
	where g.user_id = %d
	union all 
	select b.created_at,b.user_id,b.match_id,-1,-1*b.points,m.home,m.away
	FROM public.bets b
	left join public.matches m on b.match_id = m.ID
	where b.user_id = %d
	order by created_at desc limit %d offset %d`, userid, userid, offset, (page-1)*offset)

	err := GetDB().Raw(s).Scan(&transactions).Error
	if err != nil {
		return nil
	}
	return transactions
}
