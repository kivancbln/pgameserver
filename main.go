package main

import (
	"encoding/json"
	"fmt"

	"net/http"
	"os"
	"predictiongame/app"
	"predictiongame/controllers"
	"predictiongame/models"
	"time"

	"github.com/go-chi/chi"
	"github.com/gorilla/handlers"
	"github.com/gorilla/websocket"
	"github.com/robfig/cron"
	uuid "github.com/satori/go.uuid"
	httpSwagger "github.com/swaggo/http-swagger"
	_ "github.com/swaggo/http-swagger/example/go-chi/docs"
	//"log"
	//"github.com/rs/cors"
)

func corsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		allowHeaders := "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization"

		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "http://localhost:8100")
		w.Header().Set("Access-Control-Allow-Methods", "POST, PUT, PATCH, GET, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Authorization")
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		w.Header().Set("Access-Control-Allow-Headers", allowHeaders)

		next.ServeHTTP(w, r)
	})
}

type ClientManager struct {
	clients    map[*Client]bool
	broadcast  chan []byte
	register   chan *Client
	unregister chan *Client
}

type Client struct {
	id     string
	socket *websocket.Conn
	send   chan []byte
}

type Message struct {
	Sender    string `json:"sender,omitempty"`
	Recipient string `json:"recipient,omitempty"`
	Content   string `json:"content,omitempty"`
}

var manager = ClientManager{
	broadcast:  make(chan []byte),
	register:   make(chan *Client),
	unregister: make(chan *Client),
	clients:    make(map[*Client]bool),
}

func (manager *ClientManager) start() {
	for {
		select {
		case conn := <-manager.register:
			manager.clients[conn] = true
			jsonMessage, _ := json.Marshal(&Message{Content: "/A new socket has connected."})
			fmt.Println("A new socket has connected.")
			manager.send(jsonMessage, conn)
		case conn := <-manager.unregister:
			if _, ok := manager.clients[conn]; ok {
				close(conn.send)
				delete(manager.clients, conn)
				jsonMessage, _ := json.Marshal(&Message{Content: "/A socket has disconnected."})
				fmt.Println("A new socket has disconnected.")

				manager.send(jsonMessage, conn)
			}
		case message := <-manager.broadcast:
			for conn := range manager.clients {
				select {
				case conn.send <- message:
				default:
					close(conn.send)
					delete(manager.clients, conn)
				}
			}
		}
	}
}

func (manager *ClientManager) send(message []byte, ignore *Client) {
	fmt.Println(string(message))
	for conn := range manager.clients {
		if conn != ignore {
			conn.send <- message
		}
	}
}

func (c *Client) read() {
	defer func() {
		manager.unregister <- c
		c.socket.Close()
	}()

	for {
		_, message, err := c.socket.ReadMessage()
		fmt.Println(string(message))

		if err != nil {
			manager.unregister <- c
			c.socket.Close()
			break
		}
		jsonMessage, _ := json.Marshal(&Message{Sender: c.id, Content: string(message)})
		manager.broadcast <- jsonMessage
	}
}

func (c *Client) write() {
	defer func() {
		c.socket.Close()
	}()

	for {
		select {
		case message, ok := <-c.send:
			if !ok {
				c.socket.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			c.socket.WriteMessage(websocket.TextMessage, message)
		}
	}
}

func wsPage(res http.ResponseWriter, req *http.Request) {
	conn, error := (&websocket.Upgrader{CheckOrigin: func(r *http.Request) bool { return true }}).Upgrade(res, req, nil)
	if error != nil {
		http.NotFound(res, req)
		return
	}

	client := &Client{id: uuid.NewV4().String(), socket: conn, send: make(chan []byte)}

	manager.register <- client

	go client.read()
	go client.write()
}

func main() {

	c := cron.New()
	fmt.Println("Create new cron")
	c.AddFunc("0 30 2 * * *", func() {
		fmt.Println("finishing matches")
		t := time.Now()
		date := t.AddDate(0, 0, -1).Format("2006-01-02")

		models.FetchMatchesFromApiAndFinish(date)
		models.SendNotifications(date, t.Format("2006-01-02"))
	})

	c.AddFunc("0 45 2 * * *", func() {
		fmt.Println("finishing challenges")

		models.FinishActiveChallenges()
	})

	c.AddFunc("0 0 3 * * *", func() {
		fmt.Println("fetching new matches")
		t := time.Now()
		date := t.AddDate(0, 0, 3).Format("2006-01-02")

		models.FetchMatchesFromApi(date)
	})

	c.AddFunc("0 45 3 * * *", func() {
		fmt.Println("auto creating challenges")
		t := time.Now()
		date1 := t.Format("2006-01-02")
		date2 := t.AddDate(0, 0, 1).Format("2006-01-02")

		models.AutoCreateChallenges(date1, date2)
	})

	c.Start()

	fmt.Println(time.Now())
	router := chi.NewRouter()

	//router.HandleFunc("/api/user/new", controllers.CreateAccount).Methods("POST")
	//router.HandleFunc("/api/user/login", controllers.Authenticate).Methods("POST")
	//router.HandleFunc("/api/contacts/new", controllers.CreateContact).Methods("POST")
	//router.HandleFunc("/api/me/contacts", controllers.GetContactsFor).Methods("GET") //  user/2/contacts

	router.Use(app.JwtAuthentication) //attach JWT auth middleware
	router.Get("/api/me/contacts", controllers.GetContactsFor)
	router.Post("/api/user/new", controllers.CreateAccount)
	router.Post("/api/user/login", controllers.Authenticate)
	router.Get("/api/user/getuser", controllers.GetUser)
	router.Post("/api/user/getbyemail", controllers.GetUserByEmail)
	router.Post("/api/user/getbyuuid", controllers.GetUserByUUID)

	router.Post("/api/account/confirmorcancelguest", controllers.ConfirmOrCancel)
	router.Post("/api/account/updateusername", controllers.UpdateUserName)
	router.Post("/api/account/updatesubscriptionid", controllers.UpdateSubscriptionId)
	router.Post("/api/account/updatereferenceid", controllers.UpdateReferenceID)

	router.Get("/api/notification/list", controllers.GetNotifications)
	router.Post("/api/notification/new", controllers.CreateNotification)
	router.Post("/api/notification/read", controllers.ReadNotification)

	router.Get("/api/match/list", controllers.GetMatches)
	router.Get("/api/match/listbydate", controllers.GetMatchesByDate)
	router.Get("/api/match/getfinishedmatches", controllers.GetFinishedMatches)
	router.Get("/api/match/getfinishedmatchesbydate", controllers.GetFinishedMatchesByDate)
	router.Get("/api/match/getbyid", controllers.GetMatchById)
	router.Get("/api/match/getrankings", controllers.GetRankings)
	router.Post("/api/match/new", controllers.CreateMatch)
	router.Post("/api/match/finish", controllers.FinishMatch)
	router.Post("/api/match/finish2", controllers.FinishMatch2)
	router.Post("/api/match/newmatchodds", controllers.CreateMatchOdds)

	router.Post("/api/action/new", controllers.CreateActionLog)

	router.Get("/api/bet/list", controllers.GetPreviousBetsByUserByMatch)
	router.Get("/api/bet/listbyuser", controllers.GetPreviousBetsByUser)
	router.Get("/api/bet/getactivebets", controllers.GetActiveBets)
	router.Get("/api/bet/getfinishedbets", controllers.GetFinishedBets)

	router.Post("/api/bet/new", controllers.CreateBetByUserByMatch)
	router.Delete("/api/bet/delete/{id}", controllers.DeleteBet)

	router.Get("/api/gain/getpoints", controllers.GetPoints)
	router.Get("/api/gain/transactions", controllers.GetTransactions)
	router.Post("/api/gain/new", controllers.CreateGain)

	router.Get("/api/challenge/getchallenges", controllers.GetChallenges)
	router.Get("/api/challenge/getactivechallenges", controllers.GetActiveChallenges)
	router.Get("/api/challenge/getactivechallengespaged", controllers.GetActiveChallengesPaged)
	router.Get("/api/challenge/getactivechallengesfromcache", controllers.GetActiveChallengesFromCache)
	router.Get("/api/challenge/getfinishedchallengespaged", controllers.GetFinishedChallengesPaged)
	router.Get("/api/challenge/getfinishedchallenges", controllers.GetFinishedChallenges)
	router.Get("/api/challenge/getchallengebyid", controllers.GetChallengeById)
	router.Get("/api/challenge/getresultsbychallenge", controllers.GetResultsByChallenge)
	router.Post("/api/challenge/new", controllers.CreateChallenge)
	router.Post("/api/challenge/newmatches", controllers.CreateChallengeMatches)
	router.Post("/api/challenge/newcontender", controllers.CreateChallengeContender)
	router.Post("/api/challenge/checkcomplete", controllers.CheckChallengeCompleted)
	router.Post("/api/challenge/finishchallenge", controllers.FinishChallenge)

	router.Post("/api/prize/new", controllers.CreatePrize)
	router.Post("/api/prize/buyorder", controllers.CreateBuyOrder)
	router.Get("/api/prize/buyables", controllers.GetBuyables)
	router.Get("/api/prize/buyorders", controllers.GetBuyOrders)
	router.Get("/api/prize/coin", controllers.GetCoin)

	router.Post("/api/transaction/new", controllers.CreateTransaction)

	router.Get("/api/footballapi/fetchmatches", controllers.FetchMatchesFromApi)

	router.Get("/api/footballapi/fetchmatchesandfinish", controllers.FetchMatchesFromApiAndFinish)
	router.Get("/api/footballapi/fetchleaguesandsave", controllers.FetchLeaguesAndSave)

	// test to send notifications
	// models.SendNotifications("2020-04-01","2020-04-02")

	//socketio implementation

	/*server, errsocket := socketio.NewServer(nil)
	if errsocket != nil {
		fmt.Println(errsocket)
	}

	server.OnConnect("/", func(s socketio.Conn) error {
		s.SetContext("")
		fmt.Println("connected:", s.ID())
		return nil
	})
	server.OnEvent("/", "notice", func(s socketio.Conn, msg string) {
		fmt.Println("notice:", msg)
		s.Emit("reply", "have "+msg)
	})
	server.OnEvent("/chat", "msg", func(s socketio.Conn, msg string) string {
		s.SetContext(msg)
		return "recv " + msg
	})
	server.OnEvent("/", "bye", func(s socketio.Conn) string {
		last := s.Context().(string)
		s.Emit("bye", last)
		s.Close()
		return last
	})
	server.OnError("/", func(s socketio.Conn, e error) {
		fmt.Println("meet error:", e)
	})
	server.OnDisconnect("/", func(s socketio.Conn, reason string) {
		fmt.Println("closed", reason)
	})
	go server.Serve()
	defer server.Close()

	//router.Handle("/socket.io", server)
	//router.Handle("/", http.FileServer(http.Dir("docs")))
	http.Handle("/socket.io/", corsMiddleware(server))
	//fs := http.FileServer(http.Dir("docs"))
	http.Handle("/", http.FileServer(http.Dir("docs")))

	//socketio implementation
	*/

	//router.NotFoundHandler = app.NotFoundHandler

	headers := handlers.AllowedHeaders([]string{"Accept", "Authorization", "Content-Type", "Content-Length", "X-CSRF-Token", "Token", "session", "Origin", "Host", "Connection", "Accept-Encoding", "Accept-Language", "X-Requested-With"})
	methods := handlers.AllowedMethods([]string{"GET", "POST", "PUT", "HEAD", "OPTIONS", "DELETE"})
	origins := handlers.AllowedOrigins([]string{"http://localhost:8100", "http://localhost", "https://prediction-game.firebaseapp.com", "https://prediction-game.web.app", "http://192.168.8.102:8100"})
	handlers.AllowCredentials()

	//go manager.start()
	http.HandleFunc("/ws", wsPage)

	router.Get("/swagger/*", httpSwagger.Handler(
		httpSwagger.URL("http://localhost:1323/swagger/doc.json"), //The url pointing to API definition"
	))

	fs := http.FileServer(http.Dir("./index.html"))
	http.Handle("/index.html", fs)

	port := os.Getenv("PORT")
	if port == "" {
		port = "1323" //localhost
	}

	fmt.Println(port)

	err := http.ListenAndServe(":"+port, handlers.CORS(headers, methods, origins)(router)) //Launch the app, visit localhost:8000/api
	//err := http.ListenAndServeTLS(":"+port, "./openssl-1.0.2o/client.crt", "./openssl-1.0.2o/client.key", handlers.CORS(headers,methods,origins)(router))
	if err != nil {
		fmt.Print(err)
	}

}
